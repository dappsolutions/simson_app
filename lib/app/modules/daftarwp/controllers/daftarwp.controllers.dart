import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:permission_handler_platform_interface/permission_handler_platform_interface.dart';
import 'package:simson/app/modules/daftarwp/models/daftarwp.models.dart';
import 'package:http/http.dart' as http;
import 'package:simson/app/modules/version/models/version.models.dart';
import 'package:simson/app/modules/version/services/version.services.dart';
import 'package:simson/config/api.config.dart';
import 'dart:convert';

import 'package:simson/config/modules.config.dart';
import 'package:simson/config/routes.config.dart';
import 'package:simson/config/session.config.dart';
import 'package:simson/config/url.config.dart';
import 'package:url_launcher/url_launcher.dart';

// import 'package:qrscan/qrscan.dart' as scanner;

class DaftarWpController extends GetxController {
  List<DaftarWpModel> data = [];
  List<VersionModels> dataVersion = [];
  bool loadingProses = false;

  String qrcoderesult = "";
  String appVersion = '-';

  Future<void> getAppVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String appVersion = packageInfo.version;
    String appName = packageInfo.appName;
    String buildNumber = packageInfo.buildNumber;
    String packageName = packageInfo.packageName;
    String buildSignature = packageInfo.buildSignature;
    this.appVersion = appVersion;
    this.update();

    this.getAppVersionApi(appVersion);
  }

  Future<void> getAppVersionApi(String appVersion) async {
    Map<String, dynamic> params = {};

    List<VersionModels> data =
        await VersionServices.getAppVersion(params, this.dataVersion);
    this.dataVersion = data;
    if (data.isNotEmpty) {
      if (data[0].version != appVersion) {
        Get.dialog(
          AlertDialog(
            title: Text('Update Aplikasi'),
            content: Text(
                'Aplikasi Apar Versi ${appVersion} Sudah Usang, Silakan Update Aplikasi Sekarang'),
            actions: <Widget>[
              FlatButton(
                child: Text('Update Versi : ${data[0].version}'),
                onPressed: () {
                  this._launchInBrowser(UrlConfig.UPDATE_APP_URL);
                },
              ),
            ],
          ),
        );
      }
    }
    this.update();
  }

  Future<void> _launchInBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: false,
        forceWebView: false,
        headers: <String, String>{'my_header_key': 'my_header_value'},
      );
    } else {
      throw 'Could not launch $url';
    }
  }

  void getListDaftarWp() async {
    this.loadingProses = true;
    update();

    Map<String, dynamic> params = {};
    params["last_id"] =
        this.data.length > 0 ? this.data[this.data.length - 1].id : "";
    params["user_id"] = SessionConfig.user_id;

    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_PERMIT][Routes.GETDATA]),
        body: params);

    // print("REUES ${request.body}");
    if (request.statusCode == 200) {
      this.loadingProses = false;
      update();

      var data = json.decode(request.body);
      for (var val in data["data"]) {
        this.data.add(DaftarWpModel.fromJson(val));
      }
    } else {
      this.loadingProses = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }

    this.update();
  }

  void getListDaftarWpAll() async {
    this.loadingProses = true;
    update();

    Map<String, dynamic> params = {};
    params["last_id"] = "";
    params["user_id"] = SessionConfig.user_id;

    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_PERMIT][Routes.GETDATA]),
        body: params);

    // print("USER : ${SessionConfig.user_id}");
    // print("URL : ${Api.route[ModulesConfig.MODULE_PERMIT][Routes.GETDATA]}");
    // print("REUES ${request.body}");
    if (request.statusCode == 200) {
      this.loadingProses = false;
      update();

      this.data.clear();

      var data = json.decode(request.body);
      for (var val in data["data"]) {
        this.data.add(DaftarWpModel.fromJson(val));
      }
    } else {
      this.loadingProses = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }

    this.update();
  }

  Future<bool> scanQr() async {
    await Permission.camera.request();
    // String barcode = await scanner.scan();
    String barcode = "";
    if (barcode == null) {
      // print('nothing return.');
      this.qrcoderesult = "";
    } else {
      this.qrcoderesult = barcode;
    }
    this.update();

    return true;
  }
}
