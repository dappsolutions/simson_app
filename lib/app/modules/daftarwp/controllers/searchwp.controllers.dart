
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:simson/app/modules/daftarwp/models/daftarwp.models.dart';
import 'package:simson/config/modules.config.dart';
import 'package:simson/config/routes.config.dart';
import 'package:simson/config/session.config.dart';
import 'package:http/http.dart' as http;
import 'package:simson/config/api.config.dart';
import 'dart:convert';

class SearchWpControllers extends GetxController{
  List<DaftarWpModel> data = [];
  bool loadingProses = false;

  void getListDaftarWpAll(String keyword) async{
    this.loadingProses = true;
    update();

    Map<String, dynamic> params = {};
    params["keyword"] = keyword;
    params["user_id"] = SessionConfig.user_id;

    var request = await http
        .post(Uri.parse(Api.route[ModulesConfig.MODULE_PERMIT][Routes.GETDATASEARCH]), body: params);

    if(request.statusCode == 200){
      this.loadingProses = false;
      update();

      this.data.clear();

      var data = json.decode(request.body);
      print("DATA ${data.toString()}");
      for(var val in data["data"]){
        this.data.add(DaftarWpModel.fromJson(val));
      }

    }else{
      this.loadingProses = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data", backgroundColor: Colors.redAccent, colorText: Colors.white);
    }


    this.update();
  }
}