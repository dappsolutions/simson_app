

class DaftarWpModel {
  late String id;
  late String no_wp;
  late String sidak_user;
  late String latitude;
  late String longitude;
  late String jam_sidak;
  late String no_trans;
  DaftarWpModel({required this.id,required this.no_wp,required this.sidak_user,
    required this.latitude,required this.longitude,required this.jam_sidak,required this.no_trans});


  DaftarWpModel.fromJson(Map<String, dynamic> json){
    this.id = json['id'];
    this.no_wp = json['no_wp'];
    this.sidak_user = json['sidak_user'];
    this.latitude = json['latitude'];
    this.longitude = json['longitude'];
    this.jam_sidak = json['jam_sidak'];
    this.no_trans = json['no_trans'];
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['no_wp'] = this.no_wp;
    data['sidak_user'] = this.sidak_user;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['jam_sidak'] = this.jam_sidak;
    data['no_trans'] = this.no_trans;

    return data;
  }
}