import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:simson/app/modules/daftarwp/controllers/searchwp.controllers.dart';
import 'package:simson/app/modules/wpdetail/views/wpdetails.views.dart';
import 'package:simson/ui/Uicolor.dart';

class SearchWpViews extends StatefulWidget {
  @override
  _SearchWpViewsState createState() => _SearchWpViewsState();
}

class _SearchWpViewsState extends State<SearchWpViews> {
  late SearchWpControllers controllers;

  @override
  void initState() {
    controllers = new SearchWpControllers();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Uicolor.hexToColor(Uicolor.white),
      body: Container(
          child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(top: 16),
                      padding: EdgeInsets.only(top: 8, left: 8, right: 8),
                      height: MediaQuery.of(context).size.height * 0.1,
                      child: Material(
                          elevation: 0,
                          child: TextFormField(
                            autocorrect: true,
                            // controller: _controllerDua,
                            decoration: InputDecoration(
                              // prefixIcon: Icon(Icons.search),
                              hintText: 'Pencarian Wp (Lokasi Pekerjaan, Uraian Pekerjaan, No WP)',
                              suffixIcon: Icon(Icons.search, color: Uicolor.hexToColor(Uicolor.green),),
                              hintStyle: TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),
                              filled: true,
                              fillColor: Uicolor.hexToColor(Uicolor.white),
                              //
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(8.0)),
                                borderSide: BorderSide(color: Colors.white, width: 1),

                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(8.0)),
                                borderSide: BorderSide(color: Colors.white),
                              ),
                            ),
                            onFieldSubmitted: (value) async{
                                // print("DATA ${value}");
                                String keyword = value.toString();
                                controllers.getListDaftarWpAll(keyword);
                            },
                            onChanged: (value){

                            },
                            onTap: () async {

                            },
                          )
                      )
                  ),

                  Container(
                      padding: EdgeInsets.all(16),
                      child: GetBuilder<SearchWpControllers>(
                        init: controllers,
                        builder: (params){
                          if(!params.loadingProses){
                            if(params.data.length > 0){
                              return ListView.builder(
                                itemCount: params.data.length,
                                shrinkWrap: true,
                                itemBuilder: (context, index){
                                  var data = params.data[index];
                                  return Container(
                                      height: MediaQuery.of(context).size.height * 0.12,
                                      margin: EdgeInsets.only(left: 16, right: 18, top: 8),
                                      child: GestureDetector(
                                        child: Card(
                                          elevation: 0,
                                          color: Uicolor.hexToColor(Uicolor.green_smooth),
                                          child: Container(
                                            padding: EdgeInsets.all(16),
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: <Widget>[
                                                Container(
                                                    child: Icon(Icons.check_circle, color: Uicolor.hexToColor(Uicolor.green),)
                                                ),
                                                Container(
                                                  child: Text(data.no_wp),
                                                ),
                                                Container(
                                                  child: Text(data.jam_sidak),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        onTap: ()=>{
                                          Get.to(WpDetailViews(no_wp: data.no_wp, VIEW_DETAIL: false, jenis_pekerjaan: '', no_transaksi: data.no_trans,))
                                        },
                                      )
                                  );
                                },
                              );
                            }
                          }

                          if(params.data.length == 0){
                            return Center(
                              child: Text("Tidak ada data ditemukan"),
                            );
                          }

                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        },
                      )
                  ),
                ],
              )
          )
      ),
    );
  }
}
