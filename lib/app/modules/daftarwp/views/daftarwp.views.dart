import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:get/get.dart';
import 'package:simson/app/modules/daftarsidak/views/daftarsidak.views.dart';
import 'package:simson/app/modules/daftarwp/controllers/daftarwp.controllers.dart';
import 'package:simson/app/modules/daftarwp/views/dialogs/listdatapekerjaandialogssearch.dialogs.dart';
import 'package:simson/app/modules/daftarwp/views/dialogs/listdatapekerjaansidak.dialogs.dart';
import 'package:simson/app/modules/daftarwp/views/scanqr.views.dart';
import 'package:simson/app/modules/daftarwp/views/searchwp.views.dart';
import 'package:simson/app/modules/wpdetail/views/wpdetails.views.dart';
import 'package:simson/config/session.config.dart';
import 'package:simson/ui/Uicolor.dart';

class DaftarWpViews extends StatefulWidget {
  @override
  _DaftarWpViewsState createState() => _DaftarWpViewsState();
}

class _DaftarWpViewsState extends State<DaftarWpViews> {
  late DaftarWpController controller;
  

  @override
  void initState() {
    controller = new DaftarWpController();
    // controller.getListDaftarWp();
    controller.getAppVersion();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("MENU UTAMA", style: TextStyle(color: Uicolor.hexToColor(Uicolor.white)),),
        backgroundColor: Uicolor.hexToColor(Uicolor.green),
        elevation: 0,
        centerTitle: true,
        automaticallyImplyLeading: false,
        actions: <Widget>[
          // IconButton(
          //     icon: Icon(Icons.notifications, color: Uicolor.hexToColor(Uicolor.white),),
          //     onPressed: (){
          //       // Get.to(DaftarSidakViews());
          //       Get.bottomSheet(ListDataPekerjaanSidakDialogs());
          //     }
          // )
        ],
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              // Container(
              //     child: GetBuilder<DaftarWpController>(
              //       init: controller,
              //       builder: (params){
              //         if(!params.loadingProses){
              //           if(params.data.length > 0){
              //             return ListView.builder(
              //               itemCount: params.data.length,
              //               shrinkWrap: true,
              //               physics: ClampingScrollPhysics(),
              //               itemBuilder: (context, index){
              //                 var data = params.data[index];
              //                 return Container(
              //                     height: MediaQuery.of(context).size.height * 0.12,
              //                     margin: EdgeInsets.only(left: 16, right: 18, top: 8),
              //                     child: GestureDetector(
              //                       child: Card(
              //                         elevation: 0,
              //                         color: Uicolor.hexToColor(Uicolor.green_smooth),
              //                         child: Container(
              //                           padding: EdgeInsets.all(16),
              //                           child: Row(
              //                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //                             children: <Widget>[
              //                               Container(
              //                                   child: Icon(Icons.check_circle, color: Uicolor.hexToColor(Uicolor.green),)
              //                               ),
              //                               Container(
              //                                 child: Text(data.no_wp),
              //                               ),
              //                               Container(
              //                                 child: Text(data.jam_sidak),
              //                               ),
              //                             ],
              //                           ),
              //                         ),
              //                       ),
              //                       onTap: ()=>{
              //                         Get.to(WpDetailViews(no_wp: data.no_wp, VIEW_DETAIL: false,))
              //                       },
              //                     )
              //                 );
              //               },
              //             );
              //           }
              //         }
              //
              //         if(params.data.length == 0){
              //           return Center(
              //             child: Text("Tidak ada data ditemukan"),
              //           );
              //         }
              //
              //         return Center(
              //           child: CircularProgressIndicator(),
              //         );
              //       },
              //     )
              // ),
              //
              // Container(
              //   child: RaisedButton(
              //     child: Icon(Icons.refresh, color: Colors.white,),
              //     color: Uicolor.hexToColor(Uicolor.green),
              //     onPressed: (){
              //       controller.getListDaftarWp();
              //     },
              //     elevation: 3,
              //     shape:CircleBorder(),
              //   ),
              // ),

              GestureDetector(
                child: Container(
                  height: 100,
                  color: Uicolor.hexToColor(Uicolor.green_smooth),
                  margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                  padding: EdgeInsets.all(16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Icon(Icons.search, color: Colors.green,),
                      ),
                      Container(
                        child: Text("Cari Dokumen Safety Breafing", style: TextStyle(fontSize: 18, fontStyle: FontStyle.italic),),
                        margin: EdgeInsets.only(left: 16, right: 16),
                      ),
                    ],
                  ),
                ),
                onTap: (){
                  Get.bottomSheet(ListDataPekerjaanSidakSearchDialogs());
                },
              ),
              GestureDetector(
                child: Container(
                  height: 100,
                  color: Uicolor.hexToColor(Uicolor.green_smooth),
                  margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                  padding: EdgeInsets.all(16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Icon(Icons.assignment_turned_in, color: Colors.green,),
                      ),
                      Container(
                        child: Text("Dokumen Safety Breafing", style: TextStyle(fontSize: 18, fontStyle: FontStyle.italic),),
                        margin: EdgeInsets.only(left: 16, right: 16),
                      ),
                    ],
                  ),
                ),
                onTap: (){
                  Get.bottomSheet(ListDataPekerjaanSidakDialogs());
                },
              ),
              GestureDetector(
                child: Container(
                  height: 100,
                  color: Uicolor.hexToColor(Uicolor.green_smooth),
                  margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                  padding: EdgeInsets.all(16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Icon(Icons.settings_overscan, color: Colors.green,),
                      ),
                      Container(
                        child: Text("Scan Working Permit", style: TextStyle(fontSize: 18, fontStyle: FontStyle.italic),),
                        margin: EdgeInsets.only(left: 16, right: 16),
                      ),


                    ],
                  ),
                ),
                onTap: () async{
                  // bool scan = await controller.scanQr();
                  // if(scan){
                    // if(controller.qrcoderesult == ""){
                    //   Get.snackbar("Informasi", "QR Tidak ditemukan", backgroundColor: Colors.orangeAccent);
                    // }else{
                      // Get.to(WpDetailViews(no_wp: controller.qrcoderesult,VIEW_DETAIL: false, jenis_pekerjaan: '', no_transaksi: '',));
                    // }
                  // }

                  Get.to(ScanQrViews());
                },
              ),
            ],
          )
        ),
      ),
      // floatingActionButton: SpeedDial(
      //   backgroundColor: Colors.orangeAccent,
      //   children: [
      //     SpeedDialChild(
      //         backgroundColor: Colors.orangeAccent,
      //       child: Icon(Icons.refresh),
      //       onTap: (){
      //         controller.getListDaftarWpAll();
      //       }
      //     ),
      //     SpeedDialChild(
      //         backgroundColor: Colors.orangeAccent,
      //       child: Icon(Icons.settings_overscan),
      //       onTap: () async{
      //         bool scan = await controller.scanQr();
      //         if(scan){
      //           if(controller.qrcoderesult == ""){
      //             Get.snackbar("Informasi", "QR Tidak ditemukan", backgroundColor: Colors.orangeAccent);
      //           }else{
      //             Get.to(WpDetailViews(no_wp: controller.qrcoderesult,VIEW_DETAIL: false,));
      //           }
      //         }
      //
      //         // print("SCAN QR ${SessionConfig.user_id}");
      //       }
      //     ),
      //     SpeedDialChild(
      //         backgroundColor: Colors.orangeAccent,
      //       child: Icon(Icons.search),
      //       onTap: (){
      //         Get.to(SearchWpViews());
      //       }
      //     ),
      //   ],
      // )
    );
  }

}
