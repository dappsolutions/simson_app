
import 'dart:convert';

import 'package:get/get.dart';
import 'package:simple_json_mapper/simple_json_mapper.dart';
import 'package:simson/app/modules/sidak/models/apd.models.dart';
import 'package:simson/app/modules/sidak/models/conditions.models.dart';
import 'package:simson/config/api.config.dart';
import 'package:simson/config/modules.config.dart';
import 'package:simson/config/routes.config.dart';
import 'package:simson/config/session.config.dart';
import 'package:simson/mapper.g.dart' as mapper;
import 'package:http/http.dart' as http;

class CekScoreControllers extends GetxController{
  bool loadingProses = false;
  String score = "-";
  String status = "-";
  void cekScore(String no_wp, ApdModel data_apd, ConditionModel data_safety, String jenisPekerjaan) async{
    this.loadingProses = true;
    update();

    mapper.init();
    Map<String, dynamic> params = {};
    params["user_id"] = SessionConfig.user_id;
    params["no_wp"] = no_wp;
    params["jenis_pekerjaan"] = jenisPekerjaan;
    params["data_apd"] = JsonMapper.serialize(data_apd);
    params["data_safety"] = JsonMapper.serialize(data_safety);

    // print("PARAMS ${params}");

    var request = await http
        .post(Api.route[ModulesConfig.MODULE_SIDAK][Routes.CEKSCORE], body: params);

    // print("SIMPAN ${request.body}");
    if(request.statusCode == 200){
      this.loadingProses = false;
      update();

      var data = json.decode(request.body);
      this.score = data['data']['score'].toString();
      this.status = data['data']['status'].toString();
    }else{
      this.loadingProses = false;
      update();
    }
    update();
  }

}