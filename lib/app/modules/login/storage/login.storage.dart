


import 'dart:io';

import 'package:simple_json_mapper/simple_json_mapper.dart';

@JObj()
class LoginStorage {
  String username;
  String password;
  LoginStorage({required this.username,required this.password});
}