
class LoginModel{
  late String id;
  late String user;
  late String apps;
  late String createddate;

  LoginModel({required this.id,required this.user,required this.apps,required this.createddate});

  LoginModel.fromJson(Map<String, dynamic> json){
    this.id = json['id'];
    this.user = json['user'];
    this.apps = json['apps'];
    this.createddate = json['createddate'];
  }
}