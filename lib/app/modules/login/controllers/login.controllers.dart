
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_json_mapper/simple_json_mapper.dart';
import 'package:simson/app/modules/daftarwp/views/daftarwp.views.dart';
import 'package:simson/app/modules/login/models/login.models.dart';
import 'package:simson/app/modules/login/storage/login.storage.dart';
import 'package:simson/app/modules/response/models/message.models.dart';
import 'package:simson/config/api.config.dart';
import 'package:simson/config/modules.config.dart';
import 'package:simson/config/routes.config.dart';
import 'package:simson/config/session.config.dart';
import 'package:simson/mapper.g.dart' as mapper;
import 'package:http/http.dart' as http;
import 'dart:convert';

class LoginController extends GetxController{
  bool loadingLogin = false;

  @override
  void onInit() {
    super.onInit();
  }

  void signIn(username, pass, context) async{
    this.loadingLogin = true;
    update();

    mapper.init();
    var dataLogin = JsonMapper.serialize(LoginStorage(
        username: username,
        password:pass,
    ));

    Map<String, dynamic> params = {};
    params["data"] = dataLogin;

    var request = await http
        .post(Uri.parse(Api.route[ModulesConfig.MODULE_LOGIN][Routes.SIGN_IN]), body: params);

    // print("DATA ${request.body}");
    // print("LINK POSt ${Api.route[ModulesConfig.MODULE_LOGIN][Routes.SIGN_IN]}");

    if(request.statusCode == 200){
      this.loadingLogin = false;
      update();

      var data = json.decode(request.body);
      MessageModel respMessage = MessageModel.fromJson(data);
      if(respMessage.is_valid == "1"){
        LoginModel loginModel = LoginModel.fromJson(data["data"]);

        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString("user_id", loginModel.id);
        prefs.setString("user", loginModel.user);

        SessionConfig.sessionSet(loginModel.user);
        Get.snackbar("Informasi", respMessage.message, backgroundColor: Colors.green, colorText: Colors.white);
        Navigator.of(context).pop();
        Get.to(DaftarWpViews());
      }else{
        Get.snackbar("Informasi", respMessage.message, backgroundColor: Colors.redAccent, colorText: Colors.white);
      }
    }else{
      this.loadingLogin = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data", backgroundColor: Colors.redAccent, colorText: Colors.white);
    }
  }
}