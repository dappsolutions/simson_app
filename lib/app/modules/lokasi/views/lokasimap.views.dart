import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:location/location.dart';
import 'package:simson/app/modules/lokasi/controllers/lokasi.controllers.dart';

class LokasiMapViews extends StatefulWidget {
  String lat;
  String lng;
  LokasiMapViews({required this.lat,required this.lng});

  @override
  _LokasiMapViewsState createState() => _LokasiMapViewsState();
}

class _LokasiMapViewsState extends State<LokasiMapViews> {
  late LokasiController controller;

  @override
  void initState() {
    controller = new LokasiController();
    // controller.polylinePoints = PolylinePoints();
    controller.setInitialLocationWithMarker(context, double.parse(widget.lat), double.parse(widget.lng));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Container(
            child: GetBuilder<LokasiController>(
              init: controller,
              builder: (params){
                if(!params.isLoading){
                  return GoogleMap(
                    mapType: MapType.normal,
                    markers: params.markersData,
                    initialCameraPosition: CameraPosition(
                      target: LatLng(params.currentLatitudePos, params.currentLngitudePos),
                      zoom: 14,
                    ),
                    // polylines: params.polylines,
                    onMapCreated: (GoogleMapController googleMapController){
                      params.mapControl.complete(googleMapController);
                    },
                  );
                }

                return Center(child: CircularProgressIndicator(),);
              },
            )
        ),
      ),
    );
  }
}
