import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:simson/app/modules/daftarwp/views/daftarwp.views.dart';
import 'package:simson/app/modules/login/views/login.views.dart';
import 'package:simson/app/modules/main/controllers/main.controllers.dart';

class MainViews extends StatefulWidget {
  @override
  _MainViewsState createState() => _MainViewsState();
}

class _MainViewsState extends State<MainViews> {
  late MainControllers controllers;

  @override
  void initState() {
    controllers = new MainControllers();
    controllers.checkSudahLogin();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MainControllers>(
      init: controllers,
      builder: (params){
        if(params.isLogin){
          return DaftarWpViews();
        }

        return LoginViews();
      },
    );
  }
}
