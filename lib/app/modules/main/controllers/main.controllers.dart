
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simson/config/session.config.dart';

class MainControllers extends GetxController{
  bool isLogin = false;

  void checkSudahLogin() async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    //deveopment
    // await pref.setString("user_id", '973');
    // SessionConfig.sessionSet("973");
    //live
    await pref.setString("user_id", '1138');
    SessionConfig.sessionSet("1138");
    this.isLogin = pref.getString("user_id") != null ? true : false;
    // if(pref.getString("user_id") != null){
    //   SessionConfig.sessionSet(pref.getString("user"));
    // }
    update();
  }
}