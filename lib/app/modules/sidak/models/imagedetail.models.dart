

import 'dart:convert';

class ImageDetailModel {
  late String id;
  late var encoddecodeBase64Image;
  ImageDetailModel({required this.id,required this.encoddecodeBase64Image});

  ImageDetailModel.fromJson(Map<String, dynamic> json) {
    this.id = json['id'].toString();
    this.encoddecodeBase64Image = base64.decode(json['picture'].toString());
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['encoddecodeBase64Image'] = this.encoddecodeBase64Image;

    return data;
  }
}