


import 'dart:io';

import 'package:simple_json_mapper/simple_json_mapper.dart';

@JObj()
class ConditionModel {
  late String id;
  late String kondisi_tanah;
  late String kondisi_lingkungan;
  late String kondisi_cuaca;
  late String ketinggian_tk;
  late String tegangan_tk;
  late String lokasi;
  late String status;
  late String score;
  late String nilai_apd;
  late String resiko_pekerjaan;

  ConditionModel({
    required this.id,
    required this.kondisi_tanah,
    required this.kondisi_lingkungan,
    required this.kondisi_cuaca,
    required this.ketinggian_tk,
    required this.tegangan_tk,
    required this.lokasi,
    required this.status,
    required this.score,
    required this.nilai_apd,
    required this.resiko_pekerjaan,
  });


  ConditionModel.fromJson(Map<String, dynamic> json){
    this.id = json['id'];
    this.kondisi_tanah = json['kondisi_tanah'];
    this.kondisi_lingkungan = json['kondisi_lingkungan'];
    this.kondisi_cuaca = json['kondisi_cuaca'];
    this.ketinggian_tk = json['ketinggian_tk'];
    this.tegangan_tk = json['tegangan_tk'];
    this.lokasi = json['lokasi'];
    this.status = json['status'];
    this.score = json['score'].toString();
    this.nilai_apd = json['nilai_apd'].toString();
    this.resiko_pekerjaan = json['resiko_pekerjaan'].toString();
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id.toString();
    data['kondisi_tanah'] = this.kondisi_tanah.toString();
    data['kondisi_lingkungan'] = this.kondisi_lingkungan.toString();
    data['kondisi_cuaca'] = this.kondisi_cuaca.toString();
    data['ketinggian_tk'] = this.ketinggian_tk.toString();
    data['tegangan_tk'] = this.tegangan_tk.toString();
    data['lokasi'] = this.lokasi.toString();
    data['status'] = this.status.toString();
    data['score'] = this.score.toString();
    data['nilai_apd'] = this.nilai_apd.toString();
    data['resiko_pekerjaan'] = this.resiko_pekerjaan.toString();

    return data;
  }
}