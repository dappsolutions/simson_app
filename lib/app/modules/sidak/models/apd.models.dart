

import 'dart:io';

import 'package:simple_json_mapper/simple_json_mapper.dart';

@JObj()
class ApdModel {
  late String id;
  late String body_harnes;
  late String lanyard;
  late String hook_or_step;
  late String pullstrap;
  late String safety_helmet;
  late String kacamata_uv;
  late String earplug;
  late String masker;
  late String wearpack;
  late String sepatu_safety;
  late String sarung_tangan;
  late String stick_grounding;
  late String grounding_cable;
  late String stick_isolasi;
  late String voltage_detector;
  late String scaffolding;
  late String tambang;
  late String tagging;
  late String pembatas_area;

  ApdModel({
    required this.id,
    required this.body_harnes,
    required this.lanyard,
    required this.hook_or_step,
    required this.pullstrap,
    required this.safety_helmet,
    required this.kacamata_uv,
    required this.earplug,
    required this.masker,
    required this.wearpack,
    required this.sepatu_safety,
    required this.sarung_tangan,
    required this.stick_grounding,
    required this.grounding_cable,
    required this.stick_isolasi,
    required this.voltage_detector,
    required this.scaffolding,
    required this.tambang,
    required this.tagging,
    required this.pembatas_area,
  });


  ApdModel.fromJson(Map<String, dynamic> json){
    this.id = json['id'];
    this.body_harnes = json['body_harnes'];
    this.lanyard = json['lanyard'];
    this.hook_or_step = json['hook_or_step'];
    this.pullstrap = json['pullstrap'];
    this.safety_helmet = json['safety_helmet'].toString();
    this.kacamata_uv = json['kacamata_uv'].toString();
    this.earplug = json['earplug'].toString();
    this.masker = json['masker'].toString();
    this.wearpack = json['wearpack'].toString();
    this.sepatu_safety = json['sepatu_safety'].toString();
    this.sarung_tangan = json['sarung_tangan'].toString();
    this.stick_grounding = json['stick_grounding'].toString();
    this.grounding_cable = json['grounding_cable'].toString();
    this.stick_isolasi = json['stick_isolasi'].toString();
    this.voltage_detector = json['voltage_detector'].toString();
    this.scaffolding = json['scaffolding'].toString();
    this.tambang = json['tambang'].toString();
    this.tagging = json['tagging'].toString();
    this.pembatas_area = json['pembatas_area'].toString();
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id.toString();
    data['body_harnes'] = this.body_harnes.toString();
    data['lanyard'] = this.lanyard.toString();
    data['hook_or_step'] = this.hook_or_step.toString();
    data['pullstrap'] = this.pullstrap.toString();
    data['safety_helmet'] = this.safety_helmet.toString();
    data['kacamata_uv'] = this.kacamata_uv.toString();
    data['earplug'] = this.earplug.toString();
    data['masker'] = this.masker.toString();
    data['wearpack'] = this.wearpack.toString();
    data['sepatu_safety'] = this.sepatu_safety.toString();
    data['sarung_tangan'] = this.sarung_tangan.toString();
    data['stick_grounding'] = this.stick_grounding.toString();
    data['grounding_cable'] = this.grounding_cable.toString();
    data['stick_isolasi'] = this.stick_isolasi.toString();
    data['voltage_detector'] = this.voltage_detector.toString();
    data['scaffolding'] = this.scaffolding.toString();
    data['tambang'] = this.tambang.toString();
    data['tagging'] = this.tagging.toString();
    data['pembatas_area'] = this.pembatas_area.toString();

    return data;
  }
}