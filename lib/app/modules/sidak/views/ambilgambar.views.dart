import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:simson/app/modules/daftarwp/views/daftarwp.views.dart';
import 'package:simson/app/modules/login/views/login.views.dart';
import 'package:simson/app/modules/sidak/controllers/image.controllers.dart';
import 'package:simson/app/modules/sidak/controllers/sidak.controllers.dart';
import 'package:simson/app/modules/sidak/models/apd.models.dart';
import 'package:simson/app/modules/sidak/models/conditions.models.dart';
import 'package:simson/app/modules/sidak/views/cekscore.views.dart';
import 'package:simson/app/modules/sidak/views/hasilsafety.views.dart';
import 'package:simson/ui/Uicolor.dart';

class AmbilGambarViews extends StatefulWidget {
  String no_wp;
  ApdModel data_apd;
  ConditionModel data_safety;
  double lat;
  double lng;
  String jenisPekerjaan;
  String no_transaksi;

  AmbilGambarViews(
      {required this.no_wp,
      required this.data_apd,
      required this.data_safety,
      required this.lat,
      required this.lng,
      required this.jenisPekerjaan,
      required this.no_transaksi});

  @override
  _AmbilGambarViewsState createState() => _AmbilGambarViewsState();
}

class _AmbilGambarViewsState extends State<AmbilGambarViews> {
  late ImageControllers controllers;

  @override
  void initState() {
    controllers = new ImageControllers();
    controllers.clearDataImg();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          Get.defaultDialog(
              title: "Konfirmasi",
              middleText: "Apakah anda yakin akan membuang data ini ?",
              textConfirm: "ya",
              textCancel: "Tidak",
              confirmTextColor: Colors.white,
              radius: 6,
              onConfirm: () async {
                Navigator.pop(context);
                int result = await controllers.batalPengajuan(
                    widget.no_transaksi, widget.no_wp);
                if (result == 1) {
                  Get.snackbar("Informasi", "Data Berhasil Dibatalkan",
                      backgroundColor: Colors.green, colorText: Colors.white);
                  await Future.delayed(Duration(seconds: 2));
                  Get.offAll(DaftarWpViews());
                } else {
                  Get.snackbar("Informasi", "Simpan Gagal",
                      backgroundColor: Colors.red, colorText: Colors.white);
                }
              });
          return true;
        },
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: Text("GAMBAR SIDAK ${widget.no_transaksi}"),
            backgroundColor: Uicolor.hexToColor(Uicolor.green),
            elevation: 0,
            centerTitle: true,
            // iconTheme: IconThemeData(
            //   color: Uicolor.hexToColor(Uicolor.black)
            // ),
          ),
          body: Container(
              child: SingleChildScrollView(
                  child: Column(
            children: <Widget>[
              GetBuilder<ImageControllers>(
                  init: controllers,
                  builder: (params) {
                    return Container(
                      alignment: Alignment.topLeft,
                      padding: EdgeInsets.all(16),
                      child: Text(
                          "Jumlah Foto Terupload : ${params.jumlahGambarDiupload}",
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold)),
                    );
                  }),
              GetBuilder<ImageControllers>(
                init: controllers,
                builder: (params) {
                  if (params.loadingSimpan) {
                    return Container(
                        margin: EdgeInsets.only(top: 16),
                        child: Center(
                          child: CircularProgressIndicator(),
                        ));
                  }

                  return Container(
                    child: Text(""),
                  );
                },
              ),
              GetBuilder<ImageControllers>(
                init: controllers,
                builder: (params) {
                  if (params.data_img.length > 0) {
                    return Container(
                        child: Column(
                      children: <Widget>[
                        ListView.builder(
                          shrinkWrap: true,
                          itemCount: params.data_img.length,
                          physics: ClampingScrollPhysics(),
                          itemBuilder: (context, item) {
                            // return Container(
                            //     child:Text(params.data_img[item])
                            // );
                            var data = params.data_img[item];
                            return Container(
                                margin: EdgeInsets.only(
                                    left: 16, right: 16, top: 8),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      child:
                                          Center(child: Image.file(data.file)),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: 8),
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.07,
                                      width: double.infinity,
                                      // height: 50,
                                      child: RaisedButton(
                                        child: Text(
                                          "UPLOAD",
                                          style: TextStyle(color: Colors.white),
                                        ),
                                        color: Colors.orangeAccent,
                                        onPressed: () async {
                                          int doUpload =
                                              await controllers.doUpload(
                                                  widget.no_transaksi,
                                                  item,
                                                  widget.no_wp);
                                          if (doUpload == 1) {
                                            Get.snackbar("Informasi",
                                                "Image ${item + 1} Berhasil Hasil Di Upload",
                                                backgroundColor:
                                                    Colors.greenAccent,
                                                colorText: Colors.white);
                                            controllers.jumlahGambarDiupload +=
                                                1;
                                            controllers.update();
                                            controllers.clearDataImgAt(item);
                                          }
                                        },
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(3.0),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: 6),
                                      child: GestureDetector(
                                        child: Icon(
                                          Icons.close,
                                          color:
                                              Uicolor.hexToColor(Uicolor.grey),
                                        ),
                                        onTap: () {
                                          //hapus
                                          controllers.clearDataImgAt(item);
                                        },
                                      ),
                                    )
                                  ],
                                ));
                          },
                        ),
                        controllers.widgetImgPick(context)
                      ],
                    ));
                  }

                  return controllers.widgetImgPick(context);
                },
              ),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                height: MediaQuery.of(context).size.height * 0.07,
                width: double.infinity,
                // height: 50,
                child: RaisedButton(
                  child: Text(
                    "SELESAI",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.orangeAccent,
                  onPressed: () async {
                    if (widget.data_apd == null) {
                      Get.snackbar("Informasi", "Data Apd Kosong, Harus Diisi",
                          backgroundColor: Colors.red);
                      return;
                    }
                    if (widget.data_safety == null) {
                      Get.snackbar("Informasi",
                          "Data Safety Condition Kosong, Harus Diisi",
                          backgroundColor: Colors.red);
                      return;
                    }

                    if (controllers.jumlahGambarDiupload == 0) {
                      Get.snackbar(
                          "Informasi", "Dokumentasi Foto Tidak Boleh Kosong",
                          backgroundColor: Colors.red);
                      return;
                    }

                    // print("BISA LANJUT");
                    int result = await controllers.selesaiProses(
                        widget.no_wp, widget.no_transaksi, widget.data_safety);
                    if (result == 1) {
                      Get.to(HasilSafetyViews(
                        no_wp: widget.no_wp,
                        data_apd: widget.data_apd,
                        data_safety: widget.data_safety,
                        jenisPekerjaan: widget.jenisPekerjaan,
                        no_transaksi: widget.no_transaksi,
                      ));
                    } else {
                      Get.snackbar("Informasi", "Simpan Gagal",
                          backgroundColor: Colors.red, colorText: Colors.white);
                    }
                  },
                  // elevation: 3,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(3.0),
                  ),
                ),
              ),
            ],
          ))),
          // floatingActionButton: FloatingActionButton(
          //   child: Icon(Icons.check),
          //   backgroundColor: Colors.orangeAccent,
          //   onPressed: () async {
          //     if (widget.data_apd == null) {
          //       Get.snackbar("Informasi", "Data Apd Kosong, Harus Diisi",
          //           backgroundColor: Colors.red);
          //       return;
          //     }
          //     if (widget.data_safety == null) {
          //       Get.snackbar(
          //           "Informasi", "Data Safety Condition Kosong, Harus Diisi",
          //           backgroundColor: Colors.red);
          //       return;
          //     }

          //     int result = await controllers.simpanGambar(
          //         widget.no_wp,
          //         widget.data_apd,
          //         widget.data_safety,
          //         widget.lat,
          //         widget.lng,
          //         widget.jenisPekerjaan,
          //         context);
          //     if (result == 1) {
          //       Get.to(HasilSafetyViews(
          //         no_wp: widget.no_wp,
          //         data_apd: widget.data_apd,
          //         data_safety: widget.data_safety,
          //         jenisPekerjaan: widget.jenisPekerjaan,
          //       ));
          //     } else {
          //       Get.snackbar("Informasi", "Sidak Gagal Disimpan",
          //           backgroundColor: Colors.redAccent, colorText: Colors.white);
          //     }
          //   },
          // ),
        ));
  }
}
