import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:simson/app/modules/sidak/controllers/detail.image.controllers.dart';
import 'package:simson/app/modules/sidak/controllers/image.controllers.dart';
import 'package:simson/ui/Uicolor.dart';

class DetailImageViews extends StatefulWidget {
  String no_wp;
  String jenis_pekerjaan;
  String no_transaksi;
  DetailImageViews({required this.no_wp,required this.jenis_pekerjaan,required this.no_transaksi});

  @override
  _DetailImageViewsState createState() => _DetailImageViewsState();
}

class _DetailImageViewsState extends State<DetailImageViews> {
  late DetailImageControllers controllers;

  @override
  void initState() {
    controllers = new DetailImageControllers();
    controllers.clearDataImg();
    // controllers.getDetailImage(widget.no_wp, widget.jenis_pekerjaan);
    controllers.getDetailImageNew(widget.no_wp, widget.jenis_pekerjaan, widget.no_transaksi);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("GAMBAR SIDAK"),
        backgroundColor: Uicolor.hexToColor(Uicolor.green),
        elevation: 0,
        centerTitle: true,
        // iconTheme: IconThemeData(
        //   color: Uicolor.hexToColor(Uicolor.black)
        // ),
      ),
      body: Container(
          child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  GetBuilder<DetailImageControllers>(
                    init: controllers,
                    builder: (params){
                      if(!params.loadingProses){
                        if(params.data_img_all.length > 0){
                          return Container(
                              child: Column(
                                children: <Widget>[
                                  ListView.builder(
                                    shrinkWrap: true,
                                    itemCount: params.data_img_all.length,
                                    physics: ClampingScrollPhysics(),
                                    itemBuilder: (context, item){
                                      // return Container(
                                      //     child:Text(params.data_img[item])
                                      // );
                                      var data = params.data_img_all[item];
                                      return Container(
                                          margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                                          child: Column(
                                            children: <Widget>[
                                              Container(
                                                child: Center(
                                                    child: Image
                                                        .memory(data.encoddecodeBase64Image)
                                                ),
                                              ),
                                            ],
                                          )
                                      );
                                    },
                                  ),
                                ],
                              )
                          );
                        }
                      }

                      if(params.loadingProses){
                          return Container(
                            margin: EdgeInsets.only(top: 120),
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          );
                      }

                      return Container(
                        child: Text(''),
                      );
                    },
                  )
                ],
              )
          )
      ),
    );
  }
}
