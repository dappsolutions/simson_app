import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:simson/app/modules/sidak/controllers/sidak.controllers.dart';
import 'package:simson/app/modules/sidak/views/detailimage.views.dart';
import 'package:simson/app/modules/sidak/views/documentinspeksi.views.dart';
import 'package:simson/app/modules/sidak/views/documentsidak.views.dart';
import 'package:simson/ui/Uicolor.dart';

class SidakDetailViews extends StatefulWidget {
  String no_wp;
  String jenisPekerjaan;
  String no_transaksi;
  SidakDetailViews({required this.no_wp,required this.jenisPekerjaan,required this.no_transaksi});

  @override
  _SidakDetailViewsState createState() => _SidakDetailViewsState();
}

class _SidakDetailViewsState extends State<SidakDetailViews> {
  late SidakControllers controllers;

  @override
  void initState() {
    controllers = new SidakControllers();
    controllers.getDetailSidak(widget.no_wp, widget.jenisPekerjaan, widget.no_transaksi);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("DETAIL SAFETY BRIEFING ${widget.no_transaksi}"),
        backgroundColor: Uicolor.hexToColor(Uicolor.green),
        elevation: 0,
        centerTitle: true,
        // iconTheme: IconThemeData(
        //   color: Uicolor.hexToColor(Uicolor.black)
        // ),
      ),
      body: Container(
          child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
                margin:
                    EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
                alignment: Alignment.topLeft,
                child: Center(
                    child: Text(widget.jenisPekerjaan,
                        style: TextStyle(
                            color: Colors.redAccent,
                            fontSize: 24,
                            fontWeight: FontWeight.bold)))),
            Container(
              color: Uicolor.hexToColor(Uicolor.grey_young),
              padding: EdgeInsets.all(16),
              child: Center(
                child: Text(
                  "SAFETY CONDITION",
                  style: TextStyle(
                      color: Uicolor.hexToColor(Uicolor.black_young),
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 16, right: 16, top: 24),
              alignment: Alignment.topLeft,
              child: Text("Kondisi Tanah",
                  style: TextStyle(
                      color: Uicolor.hexToColor(Uicolor.black_young),
                      fontSize: 14)),
            ),
            Container(
                height: MediaQuery.of(context).size.height * 0.08,
                // width: 300,
                alignment: Alignment.topRight,
                margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                child: GetBuilder<SidakControllers>(
                  init: controllers,
                  builder: (params) {
                    if (!params.loadingProses) {
                      return Text(
                        params.dataSafety.kondisi_tanah == null
                            ? '-'
                            : params.dataSafety.kondisi_tanah,
                        textAlign: TextAlign.right,
                        style:
                            TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),
                      );
                    }

                    return Text(
                      '-',
                      textAlign: TextAlign.right,
                    );
                  },
                )),
            Container(
              margin: EdgeInsets.only(left: 16, right: 16, top: 8),
              alignment: Alignment.topLeft,
              child: Text("Kondisi Lingkungan",
                  style: TextStyle(
                      color: Uicolor.hexToColor(Uicolor.black_young),
                      fontSize: 14)),
            ),
            Container(
                height: MediaQuery.of(context).size.height * 0.08,
                // width: 300,
                margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                alignment: Alignment.topRight,
                child: GetBuilder<SidakControllers>(
                  init: controllers,
                  builder: (params) {
                    if (!params.loadingProses) {
                      return Text(
                        params.dataSafety.kondisi_lingkungan == null
                            ? '-'
                            : params.dataSafety.kondisi_lingkungan,
                        textAlign: TextAlign.right,
                        style:
                            TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),
                      );
                    }

                    return Text(
                      '-',
                      textAlign: TextAlign.right,
                    );
                  },
                )),
            Container(
              margin: EdgeInsets.only(left: 16, right: 16, top: 8),
              alignment: Alignment.topLeft,
              child: Text("Kondisi Cuaca",
                  style: TextStyle(
                      color: Uicolor.hexToColor(Uicolor.black_young),
                      fontSize: 14)),
            ),
            Container(
                height: MediaQuery.of(context).size.height * 0.08,
                // width: 300,
                margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                alignment: Alignment.topRight,
                child: GetBuilder<SidakControllers>(
                  init: controllers,
                  builder: (params) {
                    if (!params.loadingProses) {
                      return Text(
                        params.dataSafety.kondisi_cuaca == null
                            ? '-'
                            : params.dataSafety.kondisi_cuaca,
                        textAlign: TextAlign.right,
                        style:
                            TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),
                      );
                    }

                    return Text(
                      '-',
                      textAlign: TextAlign.right,
                    );
                  },
                )),
            Container(
              margin: EdgeInsets.only(left: 16, right: 16, top: 8),
              alignment: Alignment.topLeft,
              child: Text("Ketinggian TK",
                  style: TextStyle(
                      color: Uicolor.hexToColor(Uicolor.black_young),
                      fontSize: 14)),
            ),
            Container(
                height: MediaQuery.of(context).size.height * 0.08,
                // width: 300,
                margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                alignment: Alignment.topRight,
                child: GetBuilder<SidakControllers>(
                  init: controllers,
                  builder: (params) {
                    if (!params.loadingProses) {
                      return Text(
                        params.dataSafety.ketinggian_tk == null
                            ? '-'
                            : params.dataSafety.ketinggian_tk,
                        textAlign: TextAlign.right,
                        style:
                            TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),
                      );
                    }

                    return Text(
                      '-',
                      textAlign: TextAlign.right,
                    );
                  },
                )),
            Container(
              margin: EdgeInsets.only(left: 16, right: 16, top: 8),
              alignment: Alignment.topLeft,
              child: Text("Tegangan TK",
                  style: TextStyle(
                      color: Uicolor.hexToColor(Uicolor.black_young),
                      fontSize: 14)),
            ),
            Container(
                height: MediaQuery.of(context).size.height * 0.08,
                // width: 300,
                margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                alignment: Alignment.topRight,
                child: GetBuilder<SidakControllers>(
                  init: controllers,
                  builder: (params) {
                    if (!params.loadingProses) {
                      return Text(
                        params.dataSafety.tegangan_tk == null
                            ? '-'
                            : params.dataSafety.tegangan_tk,
                        textAlign: TextAlign.right,
                        style:
                            TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),
                      );
                    }

                    return Text(
                      '-',
                      textAlign: TextAlign.right,
                    );
                  },
                )),
            Container(
              margin: EdgeInsets.only(left: 16, right: 16, top: 8),
              alignment: Alignment.topLeft,
              child: Text("Lokasi",
                  style: TextStyle(
                      color: Uicolor.hexToColor(Uicolor.black_young),
                      fontSize: 14)),
            ),
            Container(
                height: MediaQuery.of(context).size.height * 0.08,
                // width: 300,
                margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                alignment: Alignment.topRight,
                child: GetBuilder<SidakControllers>(
                  init: controllers,
                  builder: (params) {
                    if (!params.loadingProses) {
                      return Text(
                        params.dataSafety.lokasi == null
                            ? '-'
                            : params.dataSafety.lokasi,
                        textAlign: TextAlign.right,
                        style:
                            TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),
                      );
                    }

                    return Text(
                      '-',
                      textAlign: TextAlign.right,
                    );
                  },
                )),
            Container(
                margin:
                    EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
                alignment: Alignment.topLeft,
                child: Center(
                    child: GetBuilder<SidakControllers>(
                  init: controllers,
                  builder: (params) {
                    if (!params.loadingProses) {
                      return Text(
                        params.dataSafety.status +
                            " / SCORE : ${params.dataSafety.score}",
                        textAlign: TextAlign.right,
                        style: TextStyle(
                            color: Uicolor.hexToColor(Uicolor.green),
                            fontSize: 24,
                            fontWeight: FontWeight.bold),
                      );
                    }

                    return Text(
                      '-',
                      textAlign: TextAlign.right,
                    );
                  },
                )
                    // child: Text("AMAN", style: TextStyle(color: Uicolor.hexToColor(Uicolor.green), fontSize: 24,fontWeight: FontWeight.bold)),
                    )),
            Container(
              color: Uicolor.hexToColor(Uicolor.grey_young),
              padding: EdgeInsets.all(16),
              child: Center(
                child: Text(
                  "KELENGKAPAN & KELAYAKAN APD",
                  style: TextStyle(
                      color: Uicolor.hexToColor(Uicolor.black_young),
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Container(
                margin:
                    EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
                alignment: Alignment.topLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Body Harness",
                        style: TextStyle(
                            color: Uicolor.hexToColor(Uicolor.black_young),
                            fontSize: 14)),
                    GestureDetector(
                      child: GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (!params.loadingProses) {
                            return params.dataApd.body_harnes == "yes"
                                ? Icon(
                                    Icons.check_circle,
                                    color: Uicolor.hexToColor(Uicolor.green),
                                  )
                                : Icon(
                                    Icons.check_circle_outline,
                                    color:
                                        Uicolor.hexToColor(Uicolor.black_young),
                                  );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                      onTap: () {},
                    )
                  ],
                )),
            Container(
                margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                alignment: Alignment.topLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Lanyard",
                        style: TextStyle(
                            color: Uicolor.hexToColor(Uicolor.black_young),
                            fontSize: 14)),
                    GestureDetector(
                      child: GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (!params.loadingProses) {
                            return params.dataApd.lanyard == "yes"
                                ? Icon(
                                    Icons.check_circle,
                                    color: Uicolor.hexToColor(Uicolor.green),
                                  )
                                : Icon(
                                    Icons.check_circle_outline,
                                    color:
                                        Uicolor.hexToColor(Uicolor.black_young),
                                  );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                      onTap: () {},
                    )
                  ],
                )),
            Container(
                margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                alignment: Alignment.topLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Hook/Stepbold",
                        style: TextStyle(
                            color: Uicolor.hexToColor(Uicolor.black_young),
                            fontSize: 14)),
                    GestureDetector(
                      child: GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (!params.loadingProses) {
                            return params.dataApd.hook_or_step == "yes"
                                ? Icon(
                                    Icons.check_circle,
                                    color: Uicolor.hexToColor(Uicolor.green),
                                  )
                                : Icon(
                                    Icons.check_circle_outline,
                                    color:
                                        Uicolor.hexToColor(Uicolor.black_young),
                                  );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                      onTap: () {},
                    )
                  ],
                )),
            Container(
                margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                alignment: Alignment.topLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Pullstrap",
                        style: TextStyle(
                            color: Uicolor.hexToColor(Uicolor.black_young),
                            fontSize: 14)),
                    GestureDetector(
                      child: GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (!params.loadingProses) {
                            return params.dataApd.pullstrap == "yes"
                                ? Icon(
                                    Icons.check_circle,
                                    color: Uicolor.hexToColor(Uicolor.green),
                                  )
                                : Icon(
                                    Icons.check_circle_outline,
                                    color:
                                        Uicolor.hexToColor(Uicolor.black_young),
                                  );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                      onTap: () {},
                    )
                  ],
                )),
            Container(
                margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                alignment: Alignment.topLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Safety Helmet",
                        style: TextStyle(
                            color: Uicolor.hexToColor(Uicolor.black_young),
                            fontSize: 14)),
                    GestureDetector(
                      child: GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (!params.loadingProses) {
                            return params.dataApd.safety_helmet == "yes"
                                ? Icon(
                                    Icons.check_circle,
                                    color: Uicolor.hexToColor(Uicolor.green),
                                  )
                                : Icon(
                                    Icons.check_circle_outline,
                                    color:
                                        Uicolor.hexToColor(Uicolor.black_young),
                                  );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                      onTap: () {},
                    )
                  ],
                )),
            Container(
                margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                alignment: Alignment.topLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Kacamata uv Bening",
                        style: TextStyle(
                            color: Uicolor.hexToColor(Uicolor.black_young),
                            fontSize: 14)),
                    GestureDetector(
                      child: GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (!params.loadingProses) {
                            return params.dataApd.kacamata_uv == "yes"
                                ? Icon(
                                    Icons.check_circle,
                                    color: Uicolor.hexToColor(Uicolor.green),
                                  )
                                : Icon(
                                    Icons.check_circle_outline,
                                    color:
                                        Uicolor.hexToColor(Uicolor.black_young),
                                  );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                      onTap: () {},
                    )
                  ],
                )),
            Container(
                margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                alignment: Alignment.topLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Ear Plug",
                        style: TextStyle(
                            color: Uicolor.hexToColor(Uicolor.black_young),
                            fontSize: 14)),
                    GestureDetector(
                      child: GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (!params.loadingProses) {
                            return params.dataApd.earplug == "yes"
                                ? Icon(
                                    Icons.check_circle,
                                    color: Uicolor.hexToColor(Uicolor.green),
                                  )
                                : Icon(
                                    Icons.check_circle_outline,
                                    color:
                                        Uicolor.hexToColor(Uicolor.black_young),
                                  );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                      onTap: () {},
                    )
                  ],
                )),
            Container(
                margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                alignment: Alignment.topLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Masker",
                        style: TextStyle(
                            color: Uicolor.hexToColor(Uicolor.black_young),
                            fontSize: 14)),
                    GestureDetector(
                      child: GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (!params.loadingProses) {
                            return params.dataApd.masker == "yes"
                                ? Icon(
                                    Icons.check_circle,
                                    color: Uicolor.hexToColor(Uicolor.green),
                                  )
                                : Icon(
                                    Icons.check_circle_outline,
                                    color:
                                        Uicolor.hexToColor(Uicolor.black_young),
                                  );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                      onTap: () {},
                    )
                  ],
                )),
            Container(
                margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                alignment: Alignment.topLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Wear Pack",
                        style: TextStyle(
                            color: Uicolor.hexToColor(Uicolor.black_young),
                            fontSize: 14)),
                    GestureDetector(
                      child: GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (!params.loadingProses) {
                            return params.dataApd.wearpack == "yes"
                                ? Icon(
                                    Icons.check_circle,
                                    color: Uicolor.hexToColor(Uicolor.green),
                                  )
                                : Icon(
                                    Icons.check_circle_outline,
                                    color:
                                        Uicolor.hexToColor(Uicolor.black_young),
                                  );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                      onTap: () {},
                    )
                  ],
                )),
            Container(
                margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                alignment: Alignment.topLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Sepatu Safety",
                        style: TextStyle(
                            color: Uicolor.hexToColor(Uicolor.black_young),
                            fontSize: 14)),
                    GestureDetector(
                      child: GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (!params.loadingProses) {
                            return params.dataApd.sepatu_safety == "yes"
                                ? Icon(
                                    Icons.check_circle,
                                    color: Uicolor.hexToColor(Uicolor.green),
                                  )
                                : Icon(
                                    Icons.check_circle_outline,
                                    color:
                                        Uicolor.hexToColor(Uicolor.black_young),
                                  );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                      onTap: () {},
                    )
                  ],
                )),
            Container(
                margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                alignment: Alignment.topLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Sarung Tangan",
                        style: TextStyle(
                            color: Uicolor.hexToColor(Uicolor.black_young),
                            fontSize: 14)),
                    GestureDetector(
                      child: GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (!params.loadingProses) {
                            return params.dataApd.sarung_tangan == "yes"
                                ? Icon(
                                    Icons.check_circle,
                                    color: Uicolor.hexToColor(Uicolor.green),
                                  )
                                : Icon(
                                    Icons.check_circle_outline,
                                    color:
                                        Uicolor.hexToColor(Uicolor.black_young),
                                  );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                      onTap: () {},
                    )
                  ],
                )),
            Container(
                margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                alignment: Alignment.topLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Stick Grounding",
                        style: TextStyle(
                            color: Uicolor.hexToColor(Uicolor.black_young),
                            fontSize: 14)),
                    GestureDetector(
                      child: GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (!params.loadingProses) {
                            return params.dataApd.stick_grounding == "yes"
                                ? Icon(
                                    Icons.check_circle,
                                    color: Uicolor.hexToColor(Uicolor.green),
                                  )
                                : Icon(
                                    Icons.check_circle_outline,
                                    color:
                                        Uicolor.hexToColor(Uicolor.black_young),
                                  );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                      onTap: () {},
                    )
                  ],
                )),
            Container(
                margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                alignment: Alignment.topLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Grounding Cable",
                        style: TextStyle(
                            color: Uicolor.hexToColor(Uicolor.black_young),
                            fontSize: 14)),
                    GestureDetector(
                      child: GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (!params.loadingProses) {
                            return params.dataApd.grounding_cable == "yes"
                                ? Icon(
                                    Icons.check_circle,
                                    color: Uicolor.hexToColor(Uicolor.green),
                                  )
                                : Icon(
                                    Icons.check_circle_outline,
                                    color:
                                        Uicolor.hexToColor(Uicolor.black_young),
                                  );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                      onTap: () {},
                    )
                  ],
                )),
            Container(
                margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                alignment: Alignment.topLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Stick Isolasi",
                        style: TextStyle(
                            color: Uicolor.hexToColor(Uicolor.black_young),
                            fontSize: 14)),
                    GestureDetector(
                      child: GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (!params.loadingProses) {
                            return params.dataApd.stick_isolasi == "yes"
                                ? Icon(
                                    Icons.check_circle,
                                    color: Uicolor.hexToColor(Uicolor.green),
                                  )
                                : Icon(
                                    Icons.check_circle_outline,
                                    color:
                                        Uicolor.hexToColor(Uicolor.black_young),
                                  );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                      onTap: () {},
                    )
                  ],
                )),
            Container(
                margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                alignment: Alignment.topLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Voltage Detector",
                        style: TextStyle(
                            color: Uicolor.hexToColor(Uicolor.black_young),
                            fontSize: 14)),
                    GestureDetector(
                      child: GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (!params.loadingProses) {
                            return params.dataApd.voltage_detector == "yes"
                                ? Icon(
                                    Icons.check_circle,
                                    color: Uicolor.hexToColor(Uicolor.green),
                                  )
                                : Icon(
                                    Icons.check_circle_outline,
                                    color:
                                        Uicolor.hexToColor(Uicolor.black_young),
                                  );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                      onTap: () {},
                    )
                  ],
                )),
            Container(
                margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                alignment: Alignment.topLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Sacfolding/Tangga",
                        style: TextStyle(
                            color: Uicolor.hexToColor(Uicolor.black_young),
                            fontSize: 14)),
                    GestureDetector(
                      child: GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (!params.loadingProses) {
                            return params.dataApd.scaffolding == "yes"
                                ? Icon(
                                    Icons.check_circle,
                                    color: Uicolor.hexToColor(Uicolor.green),
                                  )
                                : Icon(
                                    Icons.check_circle_outline,
                                    color:
                                        Uicolor.hexToColor(Uicolor.black_young),
                                  );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                      onTap: () {},
                    )
                  ],
                )),
            Container(
                margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                alignment: Alignment.topLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Tambang",
                        style: TextStyle(
                            color: Uicolor.hexToColor(Uicolor.black_young),
                            fontSize: 14)),
                    GestureDetector(
                      child: GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (!params.loadingProses) {
                            return params.dataApd.tambang == "yes"
                                ? Icon(
                                    Icons.check_circle,
                                    color: Uicolor.hexToColor(Uicolor.green),
                                  )
                                : Icon(
                                    Icons.check_circle_outline,
                                    color:
                                        Uicolor.hexToColor(Uicolor.black_young),
                                  );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                      onTap: () {},
                    )
                  ],
                )),
            Container(
                margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                alignment: Alignment.topLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Tagging",
                        style: TextStyle(
                            color: Uicolor.hexToColor(Uicolor.black_young),
                            fontSize: 14)),
                    GestureDetector(
                      child: GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (!params.loadingProses) {
                            return params.dataApd.tagging == "yes"
                                ? Icon(
                                    Icons.check_circle,
                                    color: Uicolor.hexToColor(Uicolor.green),
                                  )
                                : Icon(
                                    Icons.check_circle_outline,
                                    color:
                                        Uicolor.hexToColor(Uicolor.black_young),
                                  );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                      onTap: () {},
                    )
                  ],
                )),
            Container(
                margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                alignment: Alignment.topLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Pembatas Area Kerja",
                        style: TextStyle(
                            color: Uicolor.hexToColor(Uicolor.black_young),
                            fontSize: 14)),
                    GestureDetector(
                      child: GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (!params.loadingProses) {
                            return params.dataApd.pembatas_area == "yes"
                                ? Icon(
                                    Icons.check_circle,
                                    color: Uicolor.hexToColor(Uicolor.green),
                                  )
                                : Icon(
                                    Icons.check_circle_outline,
                                    color:
                                        Uicolor.hexToColor(Uicolor.black_young),
                                  );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                      onTap: () {},
                    )
                  ],
                )),
            Container(
              margin: EdgeInsets.only(left: 16, right: 16, top: 16),
              height: MediaQuery.of(context).size.height * 0.07,
              width: double.infinity,
              // height: 50,
              child: RaisedButton(
                child: Text(
                  "DETAIL GAMBAR",
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.orangeAccent,
                onPressed: () {
                  Get.to(DetailImageViews(
                    no_wp: widget.no_wp,
                    jenis_pekerjaan: widget.jenisPekerjaan,
                    no_transaksi: widget.no_transaksi,
                  ));
                },
                elevation: 3,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(3.0),
                ),
              ),
            ),
          ],
        ),
      )),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.assignment),
        backgroundColor: Colors.orangeAccent,
        onPressed: () {
          // Get.to(DocumentSidakViews(
          //   no_wp: widget.no_wp,
          //   no_transaksi: controllers.id_transaksi,
          // ));
          Get.to(DocumentInspeksiViews(
            no_wp: widget.no_wp,
            no_transaksi: controllers.id_transaksi,
          ));
        },
      ),
    );
  }
}
