import 'package:flutter/material.dart';
// import 'package:flutter_plugin_pdf_viewer/flutter_plugin_pdf_viewer.dart';
import 'package:simson/ui/Uicolor.dart';
import 'package:url_launcher/url_launcher.dart';

class DocumentSidakViews extends StatefulWidget {
  String no_wp;
  String no_transaksi;
  DocumentSidakViews({required this.no_wp,required this.no_transaksi});

  @override
  _DocumentSidakViewsState createState() => _DocumentSidakViewsState();
}

class _DocumentSidakViewsState extends State<DocumentSidakViews> {

  String url = "http://www.pdf995.com/samples/pdf.pdf";
  String pdfasset = "sample.pdf";
  // PDFDocument _doc;
  late bool _loading;
  // Future<void> _launched;

  Future<void> _launchInBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: false,
        forceWebView: false,
        headers: <String, String>{'my_header_key': 'my_header_value'},
      );
    } else {
      throw 'Could not launch $url';
    }
  }

  _initPdf() async {
    setState(() {
      // _loading = true;
    });

    // final doc = await PDFDocument.fromURL("http://121.100.16.218/working_permit/files/berkas/dokumen_sidak/FORMULIRSWA-${widget.no_wp}.pdf");
    // String url = "192.168.1.122/working_permit";
    String url = "121.100.16.218";
    // final doc = await PDFDocument.fromURL("http://${url}/files/berkas/dokumen_sidak/FORMULIRSIMSON-${widget.no_wp}-${widget.no_transaksi}.pdf");
    // setState(() {
    //   _doc=doc;
    //   _loading = false;
    // });
  }

  @override
  void initState() {
    super.initState();
    _initPdf();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("DOKUMEN SAFETY BRIEFING"),
        backgroundColor: Uicolor.hexToColor(Uicolor.green),
        elevation: 0,
        centerTitle: true,
        // iconTheme: IconThemeData(
        //   color: Uicolor.hexToColor(Uicolor.black)
        // ),
      ),
      body: _loading ? Center(child: CircularProgressIndicator(),) : Container(),
      floatingActionButton: Container(
        height: 100,
        margin: EdgeInsets.only(bottom: 24),
        child: FloatingActionButton(
          child: Icon(Icons.file_download),
          backgroundColor: Colors.orangeAccent,
          onPressed: (){
              String url = "121.100.16.218";
              String downloadDoc = "http://${url}/files/berkas/dokumen_sidak/FORMULIRSIMSON-${widget.no_wp}-${widget.no_transaksi}.pdf";
              _launchInBrowser(downloadDoc);
          },
        )
      ),
    );
  }
}
