import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:simson/app/modules/daftarwp/views/daftarwp.views.dart';
import 'package:simson/app/modules/sidak/controllers/hasilsafety.controllers.dart';
import 'package:simson/app/modules/sidak/models/apd.models.dart';
import 'package:simson/app/modules/sidak/models/conditions.models.dart';
import 'package:simson/app/modules/sidak/views/sidakdetail.views.dart';
import 'package:simson/ui/Uicolor.dart';

class HasilSafetyViews extends StatefulWidget {
  String no_wp;
  ApdModel data_apd;
  ConditionModel data_safety;
  String jenisPekerjaan;
  String no_transaksi;

  HasilSafetyViews({required this.no_wp,required this.data_apd,required this.data_safety,required this.jenisPekerjaan,required this.no_transaksi});

  @override
  _HasilSafetyViewsState createState() => _HasilSafetyViewsState();
}

class _HasilSafetyViewsState extends State<HasilSafetyViews> {
  late HasilSafetyControllers controllers;

  @override
  void initState() {
    controllers = new HasilSafetyControllers();
    // controllers.getDetailDataWp(widget.no_wp);
    controllers.getDetailDataWpNew(widget.no_wp, widget.no_transaksi);
    controllers.cekScore(widget.no_wp, widget.data_apd, widget.data_safety, widget.jenisPekerjaan);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      // onWillPop: () async => false,
      onWillPop: () async{  
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(widget.jenisPekerjaan),
          backgroundColor: Uicolor.hexToColor(Uicolor.green),
          elevation: 0,
          centerTitle: true,
          automaticallyImplyLeading: false,
        ),
        body: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(16),
                child: GetBuilder<HasilSafetyControllers>(
                  init: controllers,
                  builder: (params){
                    if(params.dataModel != null){
                      return Text("${params.dataModel.uraian_pekerjaan}", style: TextStyle(fontSize: 20, color: Colors.green, fontWeight: FontWeight.w600),);
                    }

                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  },
                ),
              ),
              Divider(
                color: Colors.green,
              ),
              Container(
                padding: EdgeInsets.all(16),
                child: GetBuilder<HasilSafetyControllers>(
                  init: controllers,
                  builder: (params){
                    if(params.dataModel != null){
                      return Text("${params.dataModel.lokasi_pekerjaan}", style: TextStyle(fontSize: 20, color: Colors.green, fontWeight: FontWeight.w600),);
                    }

                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  },
                ),
              ),
              Divider(
                color: Colors.green,
              ),
              Container(
                alignment: Alignment.topLeft,
                child: Center(
                  child: GetBuilder<HasilSafetyControllers>(
                    init: controllers,
                    builder: (params){
                      return Text("${widget.no_wp}", style: TextStyle(fontSize: 20, color: Colors.green, fontWeight: FontWeight.w600),);
                    },
                  ),
                ),
              ),
              Container(
                alignment: Alignment.topLeft,
                child: Center(
                  child: GetBuilder<HasilSafetyControllers>(
                    init: controllers,
                    builder: (params){
                      return Text("${widget.no_transaksi}", style: TextStyle(fontSize: 20, color: Colors.green, fontWeight: FontWeight.w600),);
                    },
                  ),
                ),
              ),
              Container(
                alignment: Alignment.topLeft,
                child: Center(
                  child: GetBuilder<HasilSafetyControllers>(
                    init: controllers,
                    builder: (params){
                      if(!params.loadingProses){
                        if(params.status == 'AMAN'){
                          return Text("SCORE : ${params.score} / ${params.status}", style: TextStyle(fontSize: 20, color: Colors.green, fontWeight: FontWeight.w600),);
                        }

                        return Text("SCORE : ${params.score} / ${params.status}", style: TextStyle(fontSize: 28, color: Colors.redAccent, fontWeight: FontWeight.w600),);
                      }
                      return CircularProgressIndicator();
                    },
                  ),
                ),
              ),
              Divider(color: Colors.green,),
              Container(
                padding: EdgeInsets.all(16),
                alignment: Alignment.topLeft,
                child: Center(
                  child: GetBuilder<HasilSafetyControllers>(
                    init: controllers,
                    builder: (params){
                      if(!params.loadingProses){
                        if(params.status == 'AMAN'){
                          return Text("Selamat Bekerja, Semoga Diberikan Kelancaran dan Utamakan Keselamatan ", style: TextStyle(fontSize: 18, color: Colors.grey, fontWeight: FontWeight.w600, fontStyle: FontStyle.italic),);
                        }

                        return Text("Anda Masih Dalam Risiko Bekerja ${params.status}, Lengkapi Peralatan APD Anda, Pastikan Personil DAlam Kondisi Sehat", style: TextStyle(fontSize: 24, color: Colors.grey, fontWeight: FontWeight.w600,  fontStyle: FontStyle.italic),);
                      }
                      return CircularProgressIndicator();
                    },
                  ),
                ),
              ),

              Container(
                margin: EdgeInsets.only(left: 16, right: 16),
                height: MediaQuery.of(context).size.height * 0.07,
                width: double.infinity,
                // height: 50,
                child: RaisedButton(
                  child: Text("HASIL DOKUMEN", style: TextStyle(color: Colors.white),),
                  color: Colors.orangeAccent,
                  onPressed: () async{
                    Get.to(SidakDetailViews(
                      no_wp: widget.no_wp,
                      jenisPekerjaan: widget.jenisPekerjaan,
                      no_transaksi: widget.no_transaksi,
                    ));
                  },
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(3.0),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                height: MediaQuery.of(context).size.height * 0.07,
                width: double.infinity,
                // height: 50,
                child: RaisedButton(
                  child: Text("KEMBALI", style: TextStyle(color: Colors.white),),
                  color: Colors.lightBlue,
                  onPressed: () async{
                    Get.offAll(DaftarWpViews());
                  },
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(3.0),
                  ),
                ),
              ),
            ],
          ),
        ),
      )
    );
  }
}
