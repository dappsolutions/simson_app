import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:simson/app/modules/sidak/controllers/cekscore.controllers.dart';
import 'package:simson/app/modules/sidak/models/apd.models.dart';
import 'package:simson/app/modules/sidak/models/conditions.models.dart';

class CekScoreViews extends StatefulWidget {
  String no_wp;
  ApdModel data_apd;
  ConditionModel data_safety;
  String jenisPekerjaan;

  CekScoreViews({required this.no_wp,required this.data_apd,required this.data_safety,required this.jenisPekerjaan});
  @override
  _CekScoreViewsState createState() => _CekScoreViewsState();
}

class _CekScoreViewsState extends State<CekScoreViews> {
  late CekScoreControllers controllers;

  @override
  void initState() {
    controllers = new CekScoreControllers();
    controllers.cekScore(widget.no_wp, widget.data_apd, widget.data_safety, widget.jenisPekerjaan);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: GetBuilder<CekScoreControllers>(
            init: controllers,
            builder: (params){
              if(!params.loadingProses){
                if(params.status == 'AMAN'){
                  return Text("SCORE : ${params.score} / ${params.status}", style: TextStyle(fontSize: 28, color: Colors.green, fontWeight: FontWeight.w600),);
                }

                return Text("SCORE : ${params.score} / ${params.status}", style: TextStyle(fontSize: 28, color: Colors.redAccent, fontWeight: FontWeight.w600),);
              }
              return CircularProgressIndicator();
            },
          ),
        ),
      ),
    );
  }
}
