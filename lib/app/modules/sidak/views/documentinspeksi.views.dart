import 'package:flutter/material.dart';
import 'package:simson/app/modules/sidak/controllers/documentinspeksi.controllers.dart';
import 'package:simson/config/url.config.dart';
import 'package:simson/ui/Uicolor.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'package:url_launcher/url_launcher.dart';

class DocumentInspeksiViews extends StatefulWidget {
  // const DocumentInspeksiViews({ Key? key }) : super(key: key);
  String no_wp;
  String no_transaksi;
  DocumentInspeksiViews({required this.no_wp,required this.no_transaksi});

  @override
  _DocumentInspeksiViewsState createState() => _DocumentInspeksiViewsState();
}

class _DocumentInspeksiViewsState extends State<DocumentInspeksiViews> {
  late DocumentInspeksiControllers controllers;
  final GlobalKey<SfPdfViewerState> _pdfViewerKey = GlobalKey();

  @override
  void initState() {
    controllers = new DocumentInspeksiControllers();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("DOKUMEN SAFETY BRIEFING"),
        backgroundColor: Uicolor.hexToColor(Uicolor.green),
        elevation: 0,
        centerTitle: true,
        // iconTheme: IconThemeData(
        //   color: Uicolor.hexToColor(Uicolor.black)
        // ),
      ),
      body: Container(
        child: SfPdfViewer.network(
          "${UrlConfig.BASE_URL_PATH}/files/berkas/dokumen_sidak/FORMULIRSIMSON-${widget.no_wp}-${widget.no_transaksi}.pdf",
          key: _pdfViewerKey,
        ),
      ),
      floatingActionButton: Container(
        height: 100,
        margin: EdgeInsets.only(bottom: 24),
        child: FloatingActionButton(
          child: Icon(Icons.file_download),
          backgroundColor: Colors.orangeAccent,
          onPressed: (){
              String downloadDoc = "${UrlConfig.BASE_URL_PATH}/files/berkas/dokumen_sidak/FORMULIRSIMSON-${widget.no_wp}-${widget.no_transaksi}.pdf";
              _launchInBrowser(downloadDoc);
          },
        )
      ),
    );
  }

  Future<void> _launchInBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: false,
        forceWebView: false,
        headers: <String, String>{'my_header_key': 'my_header_value'},
      );
    } else {
      throw 'Could not launch $url';
    }
  }
}