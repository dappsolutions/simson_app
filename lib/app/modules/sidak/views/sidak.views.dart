import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:simson/app/modules/sidak/controllers/sidak.controllers.dart';
import 'package:simson/app/modules/sidak/views/ambilgambar.views.dart';
import 'package:simson/ui/Uicolor.dart';

import 'cekscore.views.dart';

class SidakViews extends StatefulWidget {
  String no_wp;
  double lat;
  double lng;
  String jenisPekerjaan;
  SidakViews({required this.no_wp,required this.lat,required this.lng,required this.jenisPekerjaan});

  @override
  _SidakViewsState createState() => _SidakViewsState();
}

class _SidakViewsState extends State<SidakViews> {
  late SidakControllers controllers;

  @override
  void initState() {
    controllers = new SidakControllers();
    controllers.getDataKodisi();
    controllers.setCondition();
    controllers.setLatitudeLng(widget.lat, widget.lng);
    // print("LAT ${widget.lat.toString()} dan LNG ${widget.lng.toString()}");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("DETAIL SAFETY BRIEFING"),
        backgroundColor: Uicolor.hexToColor(Uicolor.green),
        elevation: 0,
        centerTitle: true,
        // iconTheme: IconThemeData(
        //   color: Uicolor.hexToColor(Uicolor.black)
        // ),
      ),
      body: Container(
          child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
                margin:
                    EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
                alignment: Alignment.topLeft,
                child: Center(
                    child: Text(widget.jenisPekerjaan,
                        style: TextStyle(
                            color: Colors.redAccent,
                            fontSize: 24,
                            fontWeight: FontWeight.bold)))),
            Container(
              color: Uicolor.hexToColor(Uicolor.grey_young),
              padding: EdgeInsets.all(16),
              child: Center(
                child: Text(
                  "SAFETY CONDITION",
                  style: TextStyle(
                      color: Uicolor.hexToColor(Uicolor.black_young),
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 16, right: 16, top: 24),
              alignment: Alignment.topLeft,
              child: Text("Resiko Pekerjaan",
                  style: TextStyle(
                      color: Uicolor.hexToColor(Uicolor.black_young),
                      fontSize: 14)),
            ),
            Container(
                height: MediaQuery.of(context).size.height * 0.08,
                // width: 300,
                margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                child: InputDecorator(
                  decoration:
                      const InputDecoration(border: OutlineInputBorder()),
                  child: DropdownButtonHideUnderline(
                      child: DropdownButton(
                    items: controllers.data_resiko.map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    value: controllers.default_resiko,
                    hint: Text("Pilih Resiko Pekerjaan"),
                    onChanged: (val) {
                      setState(() {
                        controllers.default_resiko = val.toString();
                        controllers.setCondition();
                      });
                      // PembelianModel.default_trans = val;
                      // _bloc.add(ChangeValueTypeTransaction());
                    },
                  )),
                )),
            Container(
              margin: EdgeInsets.only(left: 16, right: 16, top: 8),
              alignment: Alignment.topLeft,
              child: Text("Kondisi Tanah",
                  style: TextStyle(
                      color: Uicolor.hexToColor(Uicolor.black_young),
                      fontSize: 14)),
            ),
            Container(
                height: MediaQuery.of(context).size.height * 0.08,
                // width: 300,
                margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                child: InputDecorator(
                  decoration:
                      const InputDecoration(border: OutlineInputBorder()),
                  child: DropdownButtonHideUnderline(
                      child: DropdownButton(
                    items: controllers.data_kondisi.map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    value: controllers.default_konfisi,
                    hint: Text("Pilih Kondisi"),
                    onChanged: (val) {
                      setState(() {
                        controllers.default_konfisi = val.toString();
                        controllers.setCondition();
                      });
                      // PembelianModel.default_trans = val;
                      // _bloc.add(ChangeValueTypeTransaction());
                    },
                  )),
                )),
            Container(
              margin: EdgeInsets.only(left: 16, right: 16, top: 8),
              alignment: Alignment.topLeft,
              child: Text("Kondisi Lingkungan",
                  style: TextStyle(
                      color: Uicolor.hexToColor(Uicolor.black_young),
                      fontSize: 14)),
            ),
            Container(
                height: MediaQuery.of(context).size.height * 0.08,
                // width: 300,
                margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                child: InputDecorator(
                  decoration:
                      const InputDecoration(border: OutlineInputBorder()),
                  child: DropdownButtonHideUnderline(
                      child: DropdownButton(
                    items:
                        controllers.data_kondisi_lingkungan.map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    value: controllers.default_konfisi_lingkungan,
                    hint: Text("Pilih Kondisi"),
                    onChanged: (val) {
                      setState(() {
                        controllers.default_konfisi_lingkungan = val.toString();
                        controllers.setCondition();
                      });
                      // PembelianModel.default_trans = val;
                      // _bloc.add(ChangeValueTypeTransaction());
                    },
                  )),
                )),
            Container(
              margin: EdgeInsets.only(left: 16, right: 16, top: 8),
              alignment: Alignment.topLeft,
              child: Text("Kondisi Cuaca",
                  style: TextStyle(
                      color: Uicolor.hexToColor(Uicolor.black_young),
                      fontSize: 14)),
            ),
            Container(
                height: MediaQuery.of(context).size.height * 0.08,
                // width: 300,
                margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                child: InputDecorator(
                  decoration:
                      const InputDecoration(border: OutlineInputBorder()),
                  child: DropdownButtonHideUnderline(
                      child: DropdownButton(
                    items: controllers.data_kondisi_cuaca.map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    value: controllers.default_konfisi_cuaca,
                    hint: Text("Pilih Kondisi"),
                    onChanged: (val) {
                      setState(() {
                        controllers.default_konfisi_cuaca = val.toString();
                        controllers.setCondition();
                      });
                      // PembelianModel.default_trans = val;
                      // _bloc.add(ChangeValueTypeTransaction());
                    },
                  )),
                )),
            Container(
              margin: EdgeInsets.only(left: 16, right: 16, top: 8),
              alignment: Alignment.topLeft,
              child: Text("Ketinggian TK",
                  style: TextStyle(
                      color: Uicolor.hexToColor(Uicolor.black_young),
                      fontSize: 14)),
            ),
            Container(
                height: MediaQuery.of(context).size.height * 0.08,
                // width: 300,
                margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                child: InputDecorator(
                  decoration:
                      const InputDecoration(border: OutlineInputBorder()),
                  child: DropdownButtonHideUnderline(
                      child: DropdownButton(
                    items:
                        controllers.data_kondisi_ketinggian.map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    value: controllers.default_konfisi_ketinggian,
                    hint: Text("Pilih Kondisi"),
                    onChanged: (val) {
                      setState(() {
                        controllers.default_konfisi_ketinggian = val.toString();
                        controllers.setCondition();
                      });
                      // PembelianModel.default_trans = val;
                      // _bloc.add(ChangeValueTypeTransaction());
                    },
                  )),
                )),
            Container(
              margin: EdgeInsets.only(left: 16, right: 16, top: 8),
              alignment: Alignment.topLeft,
              child: Text("Tegangan TK",
                  style: TextStyle(
                      color: Uicolor.hexToColor(Uicolor.black_young),
                      fontSize: 14)),
            ),
            Container(
                height: MediaQuery.of(context).size.height * 0.08,
                // width: 300,
                margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                child: InputDecorator(
                  decoration:
                      const InputDecoration(border: OutlineInputBorder()),
                  child: DropdownButtonHideUnderline(
                      child: DropdownButton(
                    items:
                        controllers.data_kondisi_tegangan.map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    value: controllers.default_konfisi_tegangan,
                    hint: Text("Pilih Kondisi"),
                    onChanged: (val) {
                      setState(() {
                        controllers.default_konfisi_tegangan = val.toString();
                        controllers.setCondition();
                      });
                      // PembelianModel.default_trans = val;
                      // _bloc.add(ChangeValueTypeTransaction());
                    },
                  )),
                )),
            Container(
              margin: EdgeInsets.only(left: 16, right: 16, top: 8),
              alignment: Alignment.topLeft,
              child: Text("Lokasi",
                  style: TextStyle(
                      color: Uicolor.hexToColor(Uicolor.black_young),
                      fontSize: 14)),
            ),
            Container(
                height: MediaQuery.of(context).size.height * 0.08,
                // width: 300,
                margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                child: InputDecorator(
                  decoration:
                      const InputDecoration(border: OutlineInputBorder()),
                  child: DropdownButtonHideUnderline(
                      child: DropdownButton(
                    items: controllers.data_kondisi_lokasi.map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    value: controllers.default_konfisi_lokasi,
                    hint: Text("Pilih Lokasi"),
                    onChanged: (val) {
                      setState(() {
                        controllers.default_konfisi_lokasi = val.toString();
                        controllers.setCondition();
                      });
                      // PembelianModel.default_trans = val;
                      // _bloc.add(ChangeValueTypeTransaction());
                    },
                  )),
                )),
            Container(
              color: Uicolor.hexToColor(Uicolor.grey_young),
              padding: EdgeInsets.all(16),
              margin: EdgeInsets.only(top: 16),
              child: Center(
                child: Text(
                  "KELENGKAPAN & KELAYAKAN APD",
                  style: TextStyle(
                      color: Uicolor.hexToColor(Uicolor.black_young),
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            GestureDetector(
              child: Container(
                  margin:
                      EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Body Harness",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                      GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (params.CHECKED_BODY_HARNES) {
                            return Icon(
                              Icons.check_circle,
                              color: Uicolor.hexToColor(Uicolor.green),
                            );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                    ],
                  )),
              onTap: () {
                controllers.setChecked('body_harnes');
              },
            ),
            GestureDetector(
                child: Container(
                    margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                    alignment: Alignment.topLeft,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Lanyard",
                            style: TextStyle(
                                color: Uicolor.hexToColor(Uicolor.black_young),
                                fontSize: 14)),
                        GetBuilder<SidakControllers>(
                          init: controllers,
                          builder: (params) {
                            if (params.CHECKED_LANYARD) {
                              return Icon(
                                Icons.check_circle,
                                color: Uicolor.hexToColor(Uicolor.green),
                              );
                            }

                            return Icon(
                              Icons.check_circle_outline,
                              color: Uicolor.hexToColor(Uicolor.black_young),
                            );
                          },
                        )
                      ],
                    )),
                onTap: () {
                  controllers.setChecked("lanyard");
                }),
            GestureDetector(
              child: Container(
                  margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Hook/Stepbold",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                      GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (params.CHECKED_HOOK) {
                            return Icon(
                              Icons.check_circle,
                              color: Uicolor.hexToColor(Uicolor.green),
                            );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                    ],
                  )),
              onTap: () {
                controllers.setChecked("hook");
              },
            ),
            GestureDetector(
              child: Container(
                  margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Pullstrap",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                      GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (params.CHECKED_PULLSTRAP) {
                            return Icon(
                              Icons.check_circle,
                              color: Uicolor.hexToColor(Uicolor.green),
                            );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                    ],
                  )),
              onTap: () {
                controllers.setChecked("pullstrap");
              },
            ),
            GestureDetector(
              child: Container(
                  margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Safety Helmet",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                      GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (params.CHECKED_SAFETY_HELMET) {
                            return Icon(
                              Icons.check_circle,
                              color: Uicolor.hexToColor(Uicolor.green),
                            );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                    ],
                  )),
              onTap: () {
                controllers.setChecked("safety_helmet");
              },
            ),
            GestureDetector(
              child: Container(
                  margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Kacamata uv Bening",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                      GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (params.CHECKED_KACAMATA_UV) {
                            return Icon(
                              Icons.check_circle,
                              color: Uicolor.hexToColor(Uicolor.green),
                            );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                    ],
                  )),
              onTap: () {
                controllers.setChecked("kacamata_uv");
              },
            ),
            GestureDetector(
              child: Container(
                  margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Ear Plug",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                      GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (params.CHECKED_EARPLUG) {
                            return Icon(
                              Icons.check_circle,
                              color: Uicolor.hexToColor(Uicolor.green),
                            );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                    ],
                  )),
              onTap: () {
                controllers.setChecked("earplug");
              },
            ),
            GestureDetector(
              child: Container(
                  margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Masker",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                      GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (params.CHECKED_MASKER) {
                            return Icon(
                              Icons.check_circle,
                              color: Uicolor.hexToColor(Uicolor.green),
                            );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                    ],
                  )),
              onTap: () {
                controllers.setChecked("masker");
              },
            ),
            GestureDetector(
              child: Container(
                  margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Wear Pack",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                      GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (params.CHECKED_WEARPACK) {
                            return Icon(
                              Icons.check_circle,
                              color: Uicolor.hexToColor(Uicolor.green),
                            );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                    ],
                  )),
              onTap: () {
                controllers.setChecked("wearpack");
              },
            ),
            GestureDetector(
              child: Container(
                  margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Sepatu Safety",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                      GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (params.CHECKED_SEPATU_SAFETY) {
                            return Icon(
                              Icons.check_circle,
                              color: Uicolor.hexToColor(Uicolor.green),
                            );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                    ],
                  )),
              onTap: () {
                controllers.setChecked("sepatu_safety");
              },
            ),
            GestureDetector(
              child: Container(
                  margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Sarung Tangan",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                      GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (params.CHECKED_SARUNG_TANGAN) {
                            return Icon(
                              Icons.check_circle,
                              color: Uicolor.hexToColor(Uicolor.green),
                            );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                    ],
                  )),
              onTap: () {
                controllers.setChecked("sarung_tangan");
              },
            ),
            GestureDetector(
              child: Container(
                  margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Stick Grounding",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                      GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (params.CHECKED_STICK_GROUND) {
                            return Icon(
                              Icons.check_circle,
                              color: Uicolor.hexToColor(Uicolor.green),
                            );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                    ],
                  )),
              onTap: () {
                controllers.setChecked("stick_ground");
              },
            ),
            GestureDetector(
              child: Container(
                  margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Grounding Cable",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                      GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (params.CHECKED_GROUND_CABLE) {
                            return Icon(
                              Icons.check_circle,
                              color: Uicolor.hexToColor(Uicolor.green),
                            );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                    ],
                  )),
              onTap: () {
                controllers.setChecked("ground_cable");
              },
            ),
            GestureDetector(
              child: Container(
                  margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Stick Isolasi",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                      GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (params.CHECKED_STICK_ISOLASI) {
                            return Icon(
                              Icons.check_circle,
                              color: Uicolor.hexToColor(Uicolor.green),
                            );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                    ],
                  )),
              onTap: () {
                controllers.setChecked("stick_isolasi");
              },
            ),
            GestureDetector(
              child: Container(
                  margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Voltage Detector",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                      GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (params.CHECKED_VOLT_DETECT) {
                            return Icon(
                              Icons.check_circle,
                              color: Uicolor.hexToColor(Uicolor.green),
                            );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                    ],
                  )),
              onTap: () {
                controllers.setChecked("volt_detect");
              },
            ),
            GestureDetector(
              child: Container(
                  margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Sacfolding/Tangga",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                      GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (params.CHECKED_SCAFFOLD) {
                            return Icon(
                              Icons.check_circle,
                              color: Uicolor.hexToColor(Uicolor.green),
                            );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                    ],
                  )),
              onTap: () {
                controllers.setChecked("scaffold");
              },
            ),
            GestureDetector(
              child: Container(
                  margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Tambang",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                      GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (params.CHECKED_TAMBANG) {
                            return Icon(
                              Icons.check_circle,
                              color: Uicolor.hexToColor(Uicolor.green),
                            );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                    ],
                  )),
              onTap: () {
                controllers.setChecked("tambang");
              },
            ),
            GestureDetector(
              child: Container(
                  margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Tagging",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                      GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (params.CHECKED_TAGGING) {
                            return Icon(
                              Icons.check_circle,
                              color: Uicolor.hexToColor(Uicolor.green),
                            );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                    ],
                  )),
              onTap: () {
                controllers.setChecked("tagging");
              },
            ),
            GestureDetector(
              child: Container(
                  margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Pembatas Area Kerja",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                      GetBuilder<SidakControllers>(
                        init: controllers,
                        builder: (params) {
                          if (params.CHECKED_PEMBATAS) {
                            return Icon(
                              Icons.check_circle,
                              color: Uicolor.hexToColor(Uicolor.green),
                            );
                          }

                          return Icon(
                            Icons.check_circle_outline,
                            color: Uicolor.hexToColor(Uicolor.black_young),
                          );
                        },
                      ),
                    ],
                  )),
              onTap: () {
                controllers.setChecked("pembatas");
              },
            ),
            Container(
              margin: EdgeInsets.only(left: 16, right: 16, top: 16),
              height: MediaQuery.of(context).size.height * 0.07,
              width: double.infinity,
              // height: 50,
              child: RaisedButton(
                child: Text(
                  "CEK SCORE",
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.lightBlue,
                onPressed: () async {
                  if (this.controllers.dataApd != null &&
                      this.controllers.dataSafety != null) {
                    Get.to(CekScoreViews(
                      no_wp: widget.no_wp,
                      data_apd: this.controllers.dataApd,
                      data_safety: this.controllers.dataSafety,
                      jenisPekerjaan: widget.jenisPekerjaan,
                    ));
                  } else {
                    Get.snackbar("Informasi", "Belum ada data yang diisi",
                        backgroundColor: Colors.redAccent,
                        colorText: Colors.white);
                  }
                },
                elevation: 3,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(3.0),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 16, right: 16, top: 8),
              height: MediaQuery.of(context).size.height * 0.07,
              width: double.infinity,
              // height: 50,
              child: RaisedButton(
                child: Text(
                  "SIMPAN DATA",
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.orangeAccent,
                onPressed: () async {
                  int simpanAll = await controllers.simpanAll(widget.no_wp,
                      widget.lat, widget.lng, widget.jenisPekerjaan);
                  if (simpanAll == 1) {
                    Get.to(AmbilGambarViews(
                      no_wp: widget.no_wp,
                      data_apd: this.controllers.dataApd,
                      data_safety: this.controllers.dataSafety,
                      lat: widget.lat,
                      lng: widget.lng,
                      jenisPekerjaan: widget.jenisPekerjaan,
                      no_transaksi: controllers.no_transaksi,
                    ));
                  } else {
                    Get.snackbar("Informasi", "Sidak Gagal Disimpan",
                        backgroundColor: Colors.redAccent,
                        colorText: Colors.white);
                  }
                  // Navigator.of(context).pop();
                  // int result = await Get.to(AmbilGambarViews(
                  //   no_wp: widget.no_wp,
                  //   data_apd: this.controllers.dataApd,
                  //   data_safety: this.controllers.dataSafety,
                  //   lat: widget.lat,
                  //   lng: widget.lng,
                  //   jenisPekerjaan: widget.jenisPekerjaan,
                  // ));

                  // if (result == 1) {
                  //   Get.snackbar("Informasi", "Sidak Berhasil Disimpan",
                  //       backgroundColor: Colors.green, colorText: Colors.white);
                  // } else {
                  //   Get.snackbar("Informasi", "Sidak Gagal Disimpan",
                  //       backgroundColor: Colors.redAccent,
                  //       colorText: Colors.white);
                  // }
                },
                elevation: 3,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(3.0),
                ),
              ),
            ),
          ],
        ),
      )),
    );
  }
}
