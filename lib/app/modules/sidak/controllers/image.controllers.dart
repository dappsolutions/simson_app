
import 'dart:convert';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:simple_json_mapper/simple_json_mapper.dart';
import 'package:simson/app/modules/response/models/message.models.dart';
import 'package:simson/app/modules/sidak/models/apd.models.dart';
import 'package:simson/app/modules/sidak/models/conditions.models.dart';
import 'package:simson/app/modules/sidak/models/image.models.dart';
import 'package:simson/app/modules/sidak/models/imagedetail.models.dart';
import 'package:simson/app/modules/sidak/storage/image.storage.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:simson/config/api.config.dart';
import 'package:simson/config/modules.config.dart';
import 'package:simson/config/routes.config.dart';
import 'package:simson/config/session.config.dart';
import 'package:simson/mapper.g.dart' as mapper;
import 'package:http/http.dart' as http;
import 'package:simson/ui/Uicolor.dart';

class ImageControllers extends GetxController{

  List<ImageModel> data_img = [];
  List<ImageStorage> data_img_store = [];
  List<ImageDetailModel> data_img_all = [];

  bool loadingProses = false;
  bool loadingSimpan = false;

  int jumlahGambarDiupload = 0;

  void clearDataImg(){
    this.data_img = [];
    this.data_img_store = [];
    this.data_img_all = [];
    this.update();
  }


  void clearDataImgAt(int item) {
    this.data_img.removeAt(item);
    this.data_img_store.removeAt(item);
    this.update();
  }


  void pickImg(bool camera) async {
    // this.data_img.add("TES");
    // this.update();
    final pickedFile =
        // await ImagePicker().getImage(source: ImageSource.gallery);
        camera
            ? await ImagePicker().getImage(source: ImageSource.camera)
            : await ImagePicker().getImage(source: ImageSource.gallery);    
    if (pickedFile != null) {
      File img_from = File(pickedFile.path);

      final dir = await path_provider.getTemporaryDirectory();
      List<String> dataPath = pickedFile.path.split('/');

      String temporaryName =
          dataPath[dataPath.length - 1].replaceAll('.jpg', '');
      temporaryName = temporaryName.replaceAll('.png', '');
      temporaryName = temporaryName.replaceAll('.jpeg', '');
      temporaryName = temporaryName + "-temp.jpg";
      final targetPath = dir.absolute.path + "/${temporaryName}";

  // print("TARGET PATh ${targetPath}");
      img_from = (await FlutterImageCompress.compressAndGetFile(
        pickedFile.path,
        targetPath,
        quality: 20,
        minWidth: 300,
        minHeight: 300,
        // rotate: 90,
      ))!;

      final bytes = await img_from.readAsBytes();
      this
          .data_img
          .add(ImageModel(file: img_from, encodeImages: base64.encode(bytes)));
      this.data_img_store.add(ImageStorage(encodeImages: base64.encode(bytes)));
    }

    this.update();
  }


  Future<int> simpanGambar(String no_wp, ApdModel dataApd, ConditionModel dataSafety, double lat, double lng, String jenisPekerjaan,BuildContext context) async{
    // print("TES SIMPAN");
    int result = 0;
    this.loadingSimpan = true;
    update();

    mapper.init();
    final jsonStr = JsonMapper.serialize(this.data_img_store);

    // print('Serialized JSON:');
    // print("DATA APD ${dataApd.toJson()}");
    // print("DATA SAFETY ${dataSafety.toJson().toString()}");

    Map<String, dynamic> params = {};
    params["user_id"] = SessionConfig.user_id;
    params["no_wp"] = no_wp;
    params["jenis_pekerjaan"] = jenisPekerjaan;
    params["lat"] = lat.toString();
    params["lng"] = lng.toString();
    params["data_image"] = jsonStr;
    params["data_apd"] = JsonMapper.serialize(dataApd);
    params["data_safety"] = JsonMapper.serialize(dataSafety);

    // print("PARAMS ${params}");

    var request = await http
        .post(Api.route[ModulesConfig.MODULE_SIDAK][Routes.SIMPAN], body: params);

    // print("SIMPAN ${Api.route[ModulesConfig.MODULE_SIDAK][Routes.SIMPAN]}: ${request.body}");
    if(request.statusCode == 200){
      this.loadingSimpan = false;
      update();

      var data = json.decode(request.body);
      MessageModel respMessage = MessageModel.fromJson(data);
      if(respMessage.is_valid == "1"){
        Get.snackbar("Informasi", respMessage.message, backgroundColor: Colors.green, colorText: Colors.white);
        result = 1;
        // Navigator.of(context).pop();
      }else{
        Get.snackbar("Informasi", respMessage.message, backgroundColor: Colors.redAccent, colorText: Colors.white);
        result = 0;
      }
    }else{
      this.loadingSimpan = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data", backgroundColor: Colors.redAccent, colorText: Colors.white);
      result = 0;
    }
    update();
    return result;
    // print("REQUEST ${request.body}");
  }

  Widget choosePickImage() {
    return Scaffold(
        appBar: AppBar(
          title: Text("Ambil Gambar Melalui"),
          backgroundColor: Uicolor.hexToColor(Uicolor.green),
          elevation: 0,
          centerTitle: true,
        ),
        body: SafeArea(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(top: 16, left: 16, right: 16),
                      child: Row(
                        children: <Widget>[
                          GestureDetector(
                            child: Container(
                              child: Icon(
                                  Icons.camera_alt
                              ),
                              padding: EdgeInsets.all(16),
                            ),
                            onTap: (){
                              Get.back();
                              this.pickImg(true);
                            },
                          ),
                          Container(
                              child: Text("Kamera", style: TextStyle(fontSize: 24),)
                          ),
                        ],
                      )
                  ),
                  Divider(),
                  // Container(
                  //     margin: EdgeInsets.only(top: 16, left: 16, right: 16),
                  //     child: Row(
                  //       children: <Widget>[
                  //         GestureDetector(
                  //           child: Container(
                  //             child: Icon(
                  //                 Icons.image
                  //             ),
                  //             padding: EdgeInsets.all(16),
                  //           ),
                  //           onTap: (){
                  //             Get.back();
                  //             this.pickImg(false);
                  //           },
                  //         ),
                  //         Container(
                  //             child: Text("Gallery", style: TextStyle(fontSize: 24),)
                  //         ),
                  //       ],
                  //     )
                  // ),
                  // Divider(),
                ],
              ),
            )
        )
    );
  }

  Widget widgetImgPick(BuildContext context){
    return Container(
        padding: EdgeInsets.all(16),
        child: DottedBorder(
          dashPattern: [8,4],
          strokeWidth: 2,
          color: Colors.grey,
          child: Container(
            height: 200,
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 16, right: 16, top: 80),
                  child: Center(
                      child:  GestureDetector(
                        child: Icon(Icons.photo_camera, color: Colors.grey,),
                        onTap: (){
                          // this.pickImg();
                          Get.dialog(
                            this.choosePickImage(),
                          );
                        },
                      )
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Container(
                  margin: EdgeInsets.only(top: 6),
                  child: Center(
                    child: Text("Ambil Gambar"),
                  ),
                ),
              ],
            ),
          ),
        )
    );
  }

  void getDetailImage(String no_wp, String jenis_pekerjaan) async{
    this.loadingProses = true;
    update();

    Map<String, dynamic> params = {};
    params["no_wp"] = no_wp;
    params["jenis_pekerjaan"] = jenis_pekerjaan;

    var request = await http
        .post(Uri.parse(Api.route[ModulesConfig.MODULE_SIDAK][Routes.GET_DETAIL_IMAGE]), body: params);

    print("HASIL ${request.body}");
    if(request.statusCode == 200){
      this.loadingProses = false;
      update();

      var data = json.decode(request.body);
      // print("DATA IMAGE ${data['data']["id"]}");
      for (var item in data['data']) {
        this.data_img_all.add(ImageDetailModel.fromJson(item));
      }
    }else{
      this.loadingProses = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data", backgroundColor: Colors.redAccent, colorText: Colors.white);
    }


    this.update();
  }
  
  void getDetailImageNew(String no_wp, String jenis_pekerjaan, String no_transaksi) async{
    this.loadingProses = true;
    update();

    Map<String, dynamic> params = {};
    params["no_wp"] = no_wp;
    params["no_transaksi"] = no_transaksi;
    params["jenis_pekerjaan"] = jenis_pekerjaan;

    var request = await http
        .post(Uri.parse(Api.route[ModulesConfig.MODULE_SIDAK][Routes.GET_DETAIL_IMAGE]), body: params);

    // print("HASIL ${request.body}");
    if(request.statusCode == 200){
      this.loadingProses = false;
      update();

      var data = json.decode(request.body);
      // print("DATA IMAGE ${data['data']["id"]}");
      for (var item in data['data']) {
        this.data_img_all.add(ImageDetailModel.fromJson(item));
      }
      this.update();
    }else{
      this.loadingProses = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data", backgroundColor: Colors.redAccent, colorText: Colors.white);
    }


    this.update();
  }

  Future<int> batalPengajuan(String no_transaksi, String no_wp) async{
     int result = 0;
    this.loadingSimpan = false;
    this.update();

    Map<String, dynamic> params = {};
    params["user_id"] = SessionConfig.user_id;
    params["no_trans"] = no_transaksi;
    params["no_wp"] = no_wp;

    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_SIDAK][Routes.BATAL]),
        body: params);

    if (request.statusCode == 200) {
      this.loadingProses = false;
      update();
      var data = json.decode(request.body);
      MessageModel respMessage = MessageModel.fromJson(data);
      if (respMessage.is_valid == "1") {
        this.update();
        result = 1;
      } else {
        Get.snackbar("Informasi", "Gagal Simpan Data",
            backgroundColor: Colors.redAccent, colorText: Colors.white);
        result = 0;
      }
    } else {
      this.loadingProses = false;
      update();

      if (request.statusCode == 500) {
        Get.snackbar("Informasi", "Gagal Memuat Data ${request.body}",
            backgroundColor: Colors.redAccent, colorText: Colors.white);
      } else {
        Get.snackbar("Informasi", "Gagal Memuat Data",
            backgroundColor: Colors.redAccent, colorText: Colors.white);
      }
      result = 0;
    }

    return result;
  }

  Future<int> doUpload(String no_transaksi, int item, String no_wp) async {
    int result = 0;

    this.loadingSimpan = true;
    this.update();

    Map<String, dynamic> params = {};
    params["user_id"] = SessionConfig.user_id;
    params["no_trans"] = no_transaksi;
    params["no_wp"] = no_wp;
    params["image"] = this.data_img_store[item].encodeImages;

    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_SIDAK][Routes.SIMPANGAMBAR]),
        body: params);

    if (request.statusCode == 200) {
      this.loadingSimpan = false;
      update();
      var data = json.decode(request.body);
      MessageModel respMessage = MessageModel.fromJson(data);
      if (respMessage.is_valid == "1") {
        this.update();
        result = 1;
      } else {
        Get.snackbar("Informasi", "Gagal Simpan Data",
            backgroundColor: Colors.redAccent, colorText: Colors.white);
        result = 0;
      }
    } else {
      this.loadingSimpan = false;
      update();

      if (request.statusCode == 500) {
        Get.snackbar("Informasi", "Gagal Memuat Data ${request.body}",
            backgroundColor: Colors.redAccent, colorText: Colors.white);
      } else {
        Get.snackbar("Informasi", "Gagal Memuat Data",
            backgroundColor: Colors.redAccent, colorText: Colors.white);
      }
      result = 0;
    }

    return result;
  }

  Future<int> selesaiProses(String no_wp, String no_transaksi, ConditionModel dataSafety) async {
    int result = 0;

    this.loadingSimpan = true;
    this.update();

    Map<String, dynamic> params = {};
    params["user_id"] = SessionConfig.user_id;
    params["no_trans"] = no_transaksi;
    params["no_wp"] = no_wp;
    params["data_safety"] = JsonMapper.serialize(dataSafety);

    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_SIDAK][Routes.SELESAIPROSES]),
        body: params);

// print("DATA ${request.body}");

    if (request.statusCode == 200) {
      this.loadingSimpan = false;
      update();
      var data = json.decode(request.body);
      MessageModel respMessage = MessageModel.fromJson(data);
      if (respMessage.is_valid == "1") {
        this.update();
        result = 1;
      } else {
        Get.snackbar("Informasi", "Gagal Memproses Data",
            backgroundColor: Colors.redAccent, colorText: Colors.white);
        result = 0;
      }
    } else {
      this.loadingSimpan = false;
      update();

      if (request.statusCode == 500) {
        Get.snackbar("Informasi", "Gagal Memuat Data ${request.body}",
            backgroundColor: Colors.redAccent, colorText: Colors.white);
      } else {
        Get.snackbar("Informasi", "Gagal Memuat Data",
            backgroundColor: Colors.redAccent, colorText: Colors.white);
      }
      result = 0;
    }

    return result;
  }
}