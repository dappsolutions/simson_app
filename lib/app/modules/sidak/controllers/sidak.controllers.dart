import 'dart:convert';
import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:http/http.dart' as http;
import 'package:simson/app/modules/response/models/message.models.dart';
import 'package:simson/app/modules/sidak/models/apd.models.dart';
import 'package:simson/app/modules/sidak/models/conditions.models.dart';
import 'package:simson/app/modules/sidak/models/image.models.dart';
import 'package:simple_json_mapper/simple_json_mapper.dart';
import 'package:simson/app/modules/sidak/models/image.models.dart';
import 'package:simson/app/modules/sidak/storage/image.storage.dart';
import 'package:simson/config/api.config.dart';
import 'package:simson/config/modules.config.dart';
import 'package:simson/config/routes.config.dart';
import 'package:simson/config/session.config.dart';
import 'package:simson/mapper.g.dart' as mapper;

class SidakControllers extends GetxController {
  List<String> data_resiko = [];
  List<String> data_kondisi = [];
  List<String> data_kondisi_lingkungan = [];
  List<String> data_kondisi_cuaca = [];
  List<String> data_kondisi_ketinggian = [];
  List<String> data_kondisi_tegangan = [];
  List<String> data_kondisi_lokasi = [];
  late String default_resiko;
  late String default_konfisi;
  late String default_konfisi_lingkungan;
  late String default_konfisi_cuaca;
  late String default_konfisi_ketinggian;
  late String default_konfisi_tegangan;
  late String default_konfisi_lokasi;
  String status_akhir = "-";

  List<ImageModel> data_img = [];
  List<ImageStorage> data_img_store = [];

  bool loadingProses = false;

  late ApdModel dataApd;
  late ConditionModel dataSafety;

  String id_transaksi = "";

  //ceked
  bool CHECKED_BODY_HARNES = false;
  bool CHECKED_LANYARD = false;
  bool CHECKED_HOOK = false;
  bool CHECKED_PULLSTRAP = false;
  bool CHECKED_SAFETY_HELMET = false;
  bool CHECKED_KACAMATA_UV = false;
  bool CHECKED_EARPLUG = false;
  bool CHECKED_MASKER = false;
  bool CHECKED_WEARPACK = false;
  bool CHECKED_SEPATU_SAFETY = false;
  bool CHECKED_SARUNG_TANGAN = false;
  bool CHECKED_STICK_GROUND = false;
  bool CHECKED_GROUND_CABLE = false;
  bool CHECKED_STICK_ISOLASI = false;
  bool CHECKED_VOLT_DETECT = false;
  bool CHECKED_SCAFFOLD = false;
  bool CHECKED_TAMBANG = false;
  bool CHECKED_TAGGING = false;
  bool CHECKED_PEMBATAS = false;

  //lat lng
  double lat = 0;
  double lng = 0;

  String no_transaksi = "";

  void getDataKodisi() {
    default_resiko = 'Rendah';
    data_resiko.add("Rendah");
    data_resiko.add("Sedang");
    data_resiko.add("Tinggi");
    data_resiko.add("Sangat Tinggi");

    default_konfisi = 'Normal';
    data_kondisi.add("Normal");
    data_kondisi.add("Landai");
    data_kondisi.add("Curam");
    data_kondisi.add("Tergenang Air");

    default_konfisi_lingkungan = 'Normal';
    data_kondisi_lingkungan.add("Normal");
    data_kondisi_lingkungan.add("Paparan Fisika Debu");
    data_kondisi_lingkungan.add("Paparan Fisika UV Berlebih");
    data_kondisi_lingkungan.add("Paparan Fisika Kebisingan");
    data_kondisi_lingkungan.add("Paparan Fisika Getaran");
    data_kondisi_lingkungan.add("Paparan Kimia");
    data_kondisi_lingkungan.add("Fisika dan Kimia");

    default_konfisi_cuaca = 'Hujan';
    data_kondisi_cuaca.add("Hujan");
    data_kondisi_cuaca.add("Cerah");
    data_kondisi_cuaca.add("Mendung");
    data_kondisi_cuaca.add("Gerimis");

    default_konfisi_ketinggian = 'Tidak Pada Ketinggian';
    data_kondisi_ketinggian.add("Tidak Pada Ketinggian");
    data_kondisi_ketinggian.add("Pada Ketinggian");

    default_konfisi_tegangan = 'Bertegangan';
    data_kondisi_tegangan.add("Bertegangan");
    data_kondisi_tegangan.add("Tidak Bertegangan");

    default_konfisi_lokasi = 'Indoor';
    data_kondisi_lokasi.add("Indoor");
    data_kondisi_lokasi.add("Outdoor");
    this.update();
  }

  void cekStatus() {
    double poin_konfisi = 0;
    switch (default_konfisi) {
      case "Normal":
        poin_konfisi = 1;
        break;
      case "Landai":
        poin_konfisi = 0.98;
        break;
      case "Curam":
        poin_konfisi = 0.95;
        break;
      case "Tergenang Air":
        poin_konfisi = 0.93;
        break;
    }

    double poin_konfisi_lingkungan = 0;
    switch (default_konfisi_lingkungan) {
      case "Normal":
        poin_konfisi_lingkungan = 1;
        break;
      case "Paparan Fisika Debu":
        poin_konfisi_lingkungan = 0.95;
        break;
      case "Paparan Fisika UV Berlebih":
        poin_konfisi_lingkungan = 0.95;
        break;
      case "Paparan Fisika Kebisingan":
        poin_konfisi_lingkungan = 0.95;
        break;
      case "Paparan Fisika Getaran":
        poin_konfisi_lingkungan = 0.95;
        break;
      case "Paparan Kimia":
        poin_konfisi_lingkungan = 0.95;
        break;
      case "Fisika dan Kimia":
        poin_konfisi_lingkungan = 0.90;
        break;
    }

    double poin_kondisi_cuaca = 0;
    switch (default_konfisi_cuaca) {
      case "Mendung":
        poin_kondisi_cuaca = 0.95;
        break;
      case "Gerimis":
        poin_kondisi_cuaca = 0.98;
        break;
      case "Hujan":
        poin_kondisi_cuaca = 0.98;
        break;
      case "Cerah":
        poin_kondisi_cuaca = 1;
        break;
    }

    double poin_ketinggian_tk = 0;
    switch (default_konfisi_ketinggian) {
      case "Tidak Pada Ketinggian":
        poin_ketinggian_tk = 1;
        break;
      case "Pada Ketinggian":
        poin_ketinggian_tk = 0.95;
        break;
    }

    double poin_tegangan_tk = 0;
    switch (default_konfisi_tegangan) {
      case "Tidak Bertegangan":
        poin_tegangan_tk = 1;
        break;
      case "Bertegangan":
        poin_tegangan_tk = 0.95;
        break;
    }

    double poin_lokasi = 0;
    switch (default_konfisi_lokasi) {
      case "Indoor":
        poin_lokasi = 1;
        break;
      case "Outdoor":
        poin_lokasi = 0.98;
        break;
    }

    double total = 0;
    total = 100 *
        poin_konfisi *
        poin_konfisi_lingkungan *
        poin_kondisi_cuaca *
        poin_ketinggian_tk *
        poin_tegangan_tk;

    // print("TOTAL STATUS ${total.toString()}");
    // print("HASIL PILIH ${default_konfisi} ${poin_konfisi.toString()}, ${default_konfisi_lingkungan} ${poin_konfisi_lingkungan.toString()}, ${default_konfisi_cuaca} ${poin_kondisi_cuaca.toString()}, ${default_konfisi_ketinggian} ${poin_ketinggian_tk.toString()}, ${default_konfisi_tegangan} ${poin_tegangan_tk.toString()}, ${default_konfisi_lokasi} ${poin_lokasi.toString()}");
    if (total > 74) {
      this.status_akhir = 'AMAN';
    } else {
      this.status_akhir = 'TIDAK AMAN';
    }
    this.update();
  }

  void pickImg(bool camera) async {
    // this.data_img.add("TES");
    // this.update();
    final pickedFile =
        // await ImagePicker().getImage(source: ImageSource.gallery);
        camera
            ? await ImagePicker().getImage(source: ImageSource.camera)
            : await ImagePicker().getImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      File img_from = File(pickedFile.path);

      final dir = await path_provider.getTemporaryDirectory();
      List<String> dataPath = pickedFile.path.split('/');

      String temporaryName =
          dataPath[dataPath.length - 1].replaceAll('.jpg', '');
      temporaryName = temporaryName.replaceAll('.png', '');
      temporaryName = temporaryName.replaceAll('.jpeg', '');
      temporaryName = temporaryName + "-temp.jpg";
      final targetPath = dir.absolute.path + "/${temporaryName}";

      // print("TARGET PATh ${targetPath}");
      img_from = (await FlutterImageCompress.compressAndGetFile(
        pickedFile.path,
        targetPath,
        quality: 20,
        minWidth: 300,
        minHeight: 300,
        // rotate: 90,
      ))!;

      final bytes = await img_from.readAsBytes();
      this
          .data_img
          .add(ImageModel(file: img_from, encodeImages: base64.encode(bytes)));
      this.data_img_store.add(ImageStorage(encodeImages: base64.encode(bytes)));
    }

    this.update();
  }

  void clearDataImg() {
    this.data_img = [];
    this.data_img_store = [];
    this.update();
  }

  void clearDataImgAt(int item) {
    this.data_img.removeAt(item);
    this.data_img_store.removeAt(item);
    this.update();
  }

  simpanGambar() {
    mapper.init();
    final jsonStr = JsonMapper.serialize(this.data_img_store);
    print('Serialized JSON:');
    print(jsonStr);
  }

  Widget widgetImgPick(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(16),
        child: DottedBorder(
          dashPattern: [8, 4],
          strokeWidth: 2,
          color: Colors.grey,
          child: Container(
            height: 200,
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 16, right: 16, top: 80),
                  child: Center(
                      child: GestureDetector(
                    child: Icon(
                      Icons.photo_camera,
                      color: Colors.grey,
                    ),
                    onTap: () {
                      this.pickImg(true);
                    },
                  )),
                ),
                SizedBox(
                  height: 5,
                ),
                Container(
                  margin: EdgeInsets.only(top: 6),
                  child: Center(
                    child: Text("Ambil Gambar"),
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  void setChecked(String jenis) {
    switch (jenis) {
      case "body_harnes":
        if (this.CHECKED_BODY_HARNES) {
          this.CHECKED_BODY_HARNES = false;
        } else {
          this.CHECKED_BODY_HARNES = true;
        }
        break;
      case "lanyard":
        if (this.CHECKED_LANYARD) {
          this.CHECKED_LANYARD = false;
        } else {
          this.CHECKED_LANYARD = true;
        }
        break;
      case "hook":
        if (this.CHECKED_HOOK) {
          this.CHECKED_HOOK = false;
        } else {
          this.CHECKED_HOOK = true;
        }
        break;
      case "pullstrap":
        if (this.CHECKED_PULLSTRAP) {
          this.CHECKED_PULLSTRAP = false;
        } else {
          this.CHECKED_PULLSTRAP = true;
        }
        break;
      case "safety_helmet":
        if (this.CHECKED_SAFETY_HELMET) {
          this.CHECKED_SAFETY_HELMET = false;
        } else {
          this.CHECKED_SAFETY_HELMET = true;
        }
        break;
      case "kacamata_uv":
        if (this.CHECKED_KACAMATA_UV) {
          this.CHECKED_KACAMATA_UV = false;
        } else {
          this.CHECKED_KACAMATA_UV = true;
        }
        break;
      case "earplug":
        if (this.CHECKED_EARPLUG) {
          this.CHECKED_EARPLUG = false;
        } else {
          this.CHECKED_EARPLUG = true;
        }
        break;
      case "masker":
        if (this.CHECKED_MASKER) {
          this.CHECKED_MASKER = false;
        } else {
          this.CHECKED_MASKER = true;
        }
        break;
      case "wearpack":
        if (this.CHECKED_WEARPACK) {
          this.CHECKED_WEARPACK = false;
        } else {
          this.CHECKED_WEARPACK = true;
        }
        break;
      case "sepatu_safety":
        if (this.CHECKED_SEPATU_SAFETY) {
          this.CHECKED_SEPATU_SAFETY = false;
        } else {
          this.CHECKED_SEPATU_SAFETY = true;
        }
        break;
      case "sarung_tangan":
        if (this.CHECKED_SARUNG_TANGAN) {
          this.CHECKED_SARUNG_TANGAN = false;
        } else {
          this.CHECKED_SARUNG_TANGAN = true;
        }
        break;
      case "stick_ground":
        if (this.CHECKED_STICK_GROUND) {
          this.CHECKED_STICK_GROUND = false;
        } else {
          this.CHECKED_STICK_GROUND = true;
        }
        break;
      case "ground_cable":
        if (this.CHECKED_GROUND_CABLE) {
          this.CHECKED_GROUND_CABLE = false;
        } else {
          this.CHECKED_GROUND_CABLE = true;
        }
        break;
      case "stick_isolasi":
        if (this.CHECKED_STICK_ISOLASI) {
          this.CHECKED_STICK_ISOLASI = false;
        } else {
          this.CHECKED_STICK_ISOLASI = true;
        }
        break;
      case "volt_detect":
        if (this.CHECKED_VOLT_DETECT) {
          this.CHECKED_VOLT_DETECT = false;
        } else {
          this.CHECKED_VOLT_DETECT = true;
        }
        break;
      case "scaffold":
        if (this.CHECKED_SCAFFOLD) {
          this.CHECKED_SCAFFOLD = false;
        } else {
          this.CHECKED_SCAFFOLD = true;
        }
        break;
      case "tambang":
        if (this.CHECKED_TAMBANG) {
          this.CHECKED_TAMBANG = false;
        } else {
          this.CHECKED_TAMBANG = true;
        }
        break;
      case "tagging":
        if (this.CHECKED_TAGGING) {
          this.CHECKED_TAGGING = false;
        } else {
          this.CHECKED_TAGGING = true;
        }
        break;
      case "pembatas":
        if (this.CHECKED_PEMBATAS) {
          this.CHECKED_PEMBATAS = false;
        } else {
          this.CHECKED_PEMBATAS = true;
        }
        break;
    }

    this.dataApd = new ApdModel(
      id: "",
      body_harnes: this.CHECKED_BODY_HARNES ? "yes" : "",
      lanyard: this.CHECKED_LANYARD ? "yes" : "",
      hook_or_step: this.CHECKED_HOOK ? "yes" : "",
      pullstrap: this.CHECKED_PULLSTRAP ? "yes" : "",
      safety_helmet: this.CHECKED_SAFETY_HELMET ? "yes" : "",
      kacamata_uv: this.CHECKED_KACAMATA_UV ? "yes" : "",
      earplug: this.CHECKED_EARPLUG ? "yes" : "",
      masker: this.CHECKED_MASKER ? "yes" : "",
      wearpack: this.CHECKED_WEARPACK ? "yes" : "",
      sepatu_safety: this.CHECKED_SEPATU_SAFETY ? "yes" : "",
      sarung_tangan: this.CHECKED_SARUNG_TANGAN ? "yes" : "",
      stick_grounding: this.CHECKED_STICK_GROUND ? "yes" : "",
      grounding_cable: this.CHECKED_GROUND_CABLE ? "yes" : "",
      stick_isolasi: this.CHECKED_STICK_ISOLASI ? "yes" : "",
      voltage_detector: this.CHECKED_VOLT_DETECT ? "yes" : "",
      scaffolding: this.CHECKED_SCAFFOLD ? "yes" : "",
      tambang: this.CHECKED_TAMBANG ? "yes" : "",
      tagging: this.CHECKED_TAGGING ? "yes" : "",
      pembatas_area: this.CHECKED_PEMBATAS ? "yes" : "",
    );

    // print("DATA APD ${this.dataApd.body_harnes}");
    this.update();
  }

  void setCondition() {
    this.cekStatus();

    this.dataSafety = new ConditionModel(
        id: "",
        kondisi_tanah: this.default_konfisi,
        kondisi_lingkungan: this.default_konfisi_lingkungan,
        kondisi_cuaca: this.default_konfisi_cuaca,
        ketinggian_tk: this.default_konfisi_ketinggian,
        tegangan_tk: this.default_konfisi_tegangan,
        lokasi: this.default_konfisi_lokasi,
        status: this.status_akhir,
        resiko_pekerjaan: this.default_resiko, nilai_apd: '', score: '');

    this.update();
  }

  void setLatitudeLng(double lat, double lng) {
    this.lat = lat;
    this.lng = lng;
    update();
  }

  void getDetailSidak(
      String no_wp, String jenisPekerjaan, String no_transaksi) async {
    this.loadingProses = true;
    update();

    Map<String, dynamic> params = {};
    params["no_wp"] = no_wp;
    params["no_transaksi"] = no_transaksi;
    params["jenis_pekerjaan"] = jenisPekerjaan;

    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_SIDAK][Routes.GET_DETAIL_DATA]),
        body: params);

    // print("DATA REQUEST ${request.body}");
    if (request.statusCode == 200) {
      this.loadingProses = false;
      update();

      var data = json.decode(request.body);
      this.dataSafety = ConditionModel.fromJson(data['data_safety']);
      this.dataApd = ApdModel.fromJson(data['data_apd']);
      this.id_transaksi = data['data_safety']['permit_transaction_log'];
      this.update();
    } else {
      this.loadingProses = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }

    this.update();
  }

  Future<int> simpanAll(
      String no_wp, double lat, double lng, String jenisPekerjaan) async {
    // print("TES SIMPAN");
    int result = 0;
    this.loadingProses = true;
    update();

    mapper.init();
    final jsonStr = JsonMapper.serialize(this.data_img_store);

    // print('Serialized JSON:');
    // print("DATA APD ${dataApd.toJson()}");
    // print("DATA SAFETY ${dataSafety.toJson().toString()}");

    Map<String, dynamic> params = {};
    params["user_id"] = SessionConfig.user_id;
    params["no_wp"] = no_wp;
    params["jenis_pekerjaan"] = jenisPekerjaan;
    params["lat"] = lat.toString();
    params["lng"] = lng.toString();
    params["data_apd"] = JsonMapper.serialize(dataApd);
    params["data_safety"] = JsonMapper.serialize(dataSafety);

    // print("PARAMS ${params}");

    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_SIDAK][Routes.SIMPANALL]),
        body: params);

    // print("SIMPAN ${Api.route[ModulesConfig.MODULE_SIDAK][Routes.SIMPAN]}: ${request.body}");
    if (request.statusCode == 200) {
      this.loadingProses = false;
      update();

      var data = json.decode(request.body);
      MessageModel respMessage = MessageModel.fromJson(data);
      if (respMessage.is_valid == "1") {
        Get.snackbar("Informasi", respMessage.message,
            backgroundColor: Colors.green, colorText: Colors.white);
        this.no_transaksi = data['no_trans'];
        this.update();
        result = 1;
        // Navigator.of(context).pop();
      } else {
        Get.snackbar("Informasi", respMessage.message,
            backgroundColor: Colors.redAccent, colorText: Colors.white);
        result = 0;
      }
    } else {
      this.loadingProses = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
      result = 0;
    }
    update();
    return result;
  }
}
