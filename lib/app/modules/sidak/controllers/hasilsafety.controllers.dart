
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:simple_json_mapper/simple_json_mapper.dart';
import 'package:simson/app/modules/sidak/models/apd.models.dart';
import 'package:simson/app/modules/sidak/models/conditions.models.dart';
import 'package:simson/app/modules/wpdetail/models/detailwp.models.dart';
import 'package:simson/config/api.config.dart';
import 'package:simson/config/modules.config.dart';
import 'package:simson/config/routes.config.dart';
import 'package:simson/config/session.config.dart';
import 'package:simson/mapper.g.dart' as mapper;
import 'package:http/http.dart' as http;

class HasilSafetyControllers extends GetxController{
  bool loadingProses = false;
  String score = "-";
  String status = "-";
  DetailWpModel dataModel = DetailWpModel(
    id: '', 
    no_wp: '', 
    user: '', 
    tanggal_wp: '', 
    tipe_permit: '', 
    file_spk: '', 
    penanggung_jawab_pekerjaan: '', 
    jabatan_penanggung_jawab: '', 
    pengawas_k3: '', 
    jabatan_pengawas_k3: '', 
    pengawas_pekerjaan: '', 
    jabatan_pengawas_pekerjaan: '', 
    nama_pemohon: '', 
    perusahaan: '', 
    no_hp: '', 
    no_telp: '', 
    jabatan: '', 
    alamat: '', 
    tgl_awal: '', 
    tgl_akhir: '', 
    uraian_pekerjaan: '', 
    lokasi_pekerjaan: '', 
    tempat: '', 
    tempat_kerja_upt: '', 
    tempat_kerja_gardu: '', 
    tempat_kerja_ultg: '', 
    tempat_kerja_sutt: '', 
    tempat_kerja_bay: '', 
    tempat_kerja: '', 
    keterangan_need_sistem: '', 
    is_need_sistem: '', 
    lat: '', 
    lng: '');

  void cekScore(String no_wp, ApdModel data_apd, ConditionModel data_safety, String jenisPekerjaan) async{
    this.loadingProses = true;
    update();

    mapper.init();
    Map<String, dynamic> params = {};
    params["user_id"] = SessionConfig.user_id;
    params["no_wp"] = no_wp;
    params["jenis_pekerjaan"] = jenisPekerjaan;
    params["data_apd"] = JsonMapper.serialize(data_apd);
    params["data_safety"] = JsonMapper.serialize(data_safety);

    // print("PARAMS ${params}");

    var request = await http
        .post(Uri.parse(Api.route[ModulesConfig.MODULE_SIDAK][Routes.CEKSCORE]), body: params);

    // print("SIMPAN ${request.body}");
    if(request.statusCode == 200){
      this.loadingProses = false;
      update();

      var data = json.decode(request.body);
      this.score = data['data']['score'].toString();
      this.status = data['data']['status'].toString();
      data_safety.status = status;
      data_safety.score = score;
    }else{
      this.loadingProses = false;
      update();
    }
    update();
  }


  void getDetailDataWp(String no_wp) async{
    this.loadingProses = true;
    update();

    Map<String, dynamic> params = {};
    params["no_wp"] = no_wp;

    var request = await http
        .post(Uri.parse(Api.route[ModulesConfig.MODULE_PERMIT][Routes.GET_DETAIL_DATA]), body: params);

    if(request.statusCode == 200){
      this.loadingProses = false;
      update();

      var data = json.decode(request.body);
      dataModel = DetailWpModel.fromJson(data['data']);
    }else{
      this.loadingProses = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data", backgroundColor: Colors.redAccent, colorText: Colors.white);
    }


    this.update();
  }
  
  void getDetailDataWpNew(String no_wp, String no_transaksi) async{
    this.loadingProses = true;
    update();

    Map<String, dynamic> params = {};
    params["no_wp"] = no_wp;
    params["no_transaksi"] = no_transaksi;

    var request = await http
        .post(Uri.parse(Api.route[ModulesConfig.MODULE_PERMIT][Routes.GET_DETAIL_DATA]), body: params);

    if(request.statusCode == 200){
      this.loadingProses = false;
      update();

      var data = json.decode(request.body);
      dataModel = DetailWpModel.fromJson(data['data']);
    }else{
      this.loadingProses = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data", backgroundColor: Colors.redAccent, colorText: Colors.white);
    }


    this.update();
  }
}