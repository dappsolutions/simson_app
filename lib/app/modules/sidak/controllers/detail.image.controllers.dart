
import 'dart:convert';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:simple_json_mapper/simple_json_mapper.dart';
import 'package:simson/app/modules/response/models/message.models.dart';
import 'package:simson/app/modules/sidak/models/apd.models.dart';
import 'package:simson/app/modules/sidak/models/conditions.models.dart';
import 'package:simson/app/modules/sidak/models/image.models.dart';
import 'package:simson/app/modules/sidak/models/imagedetail.models.dart';
import 'package:simson/app/modules/sidak/storage/image.storage.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:simson/config/api.config.dart';
import 'package:simson/config/modules.config.dart';
import 'package:simson/config/routes.config.dart';
import 'package:simson/config/session.config.dart';
import 'package:simson/mapper.g.dart' as mapper;
import 'package:http/http.dart' as http;
import 'package:simson/ui/Uicolor.dart';

class DetailImageControllers extends GetxController{

  List<ImageModel> data_img = [];
  List<ImageStorage> data_img_store = [];
  List<ImageDetailModel> data_img_all = [];

  bool loadingProses = false;
  bool loadingSimpan = false;

  int jumlahGambarDiupload = 0;

  void clearDataImg(){
    this.data_img = [];
    this.data_img_store = [];
    this.data_img_all = [];
    this.update();
  }


  void clearDataImgAt(int item) {
    this.data_img.removeAt(item);
    this.data_img_store.removeAt(item);
    this.update();
  }

  
  void getDetailImageNew(String no_wp, String jenis_pekerjaan, String no_transaksi) async{
    this.loadingProses = true;
    update();

    Map<String, dynamic> params = {};
    params["no_wp"] = no_wp;
    params["no_transaksi"] = no_transaksi;
    params["jenis_pekerjaan"] = jenis_pekerjaan;

    var request = await http
        .post(Uri.parse(Api.route[ModulesConfig.MODULE_SIDAK][Routes.GET_DETAIL_IMAGE]), body: params);

    // print("HASIL ${request.body}");
    if(request.statusCode == 200){
      this.loadingProses = false;
      update();

      var data = json.decode(request.body);
      // print("DATA IMAGE ${data['data']["id"]}");
      for (var item in data['data']) {
        this.data_img_all.add(ImageDetailModel.fromJson(item));
      }
      this.update();
    }else{
      this.loadingProses = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data", backgroundColor: Colors.redAccent, colorText: Colors.white);
    }


    this.update();
  }
}