

import 'dart:io';

import 'package:simple_json_mapper/simple_json_mapper.dart';

@JObj()
class ImageStorage {
  String encodeImages;
  ImageStorage({required this.encodeImages});
}