

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:simson/app/modules/daftarwp/models/daftarwp.models.dart';
import 'package:simson/config/modules.config.dart';
import 'package:simson/config/routes.config.dart';
import 'package:simson/config/session.config.dart';
import 'package:http/http.dart' as http;
import 'package:simson/config/api.config.dart';
import 'dart:convert';

class SearchSidakControllers extends GetxController{
  List<DaftarWpModel> data = [];
  bool loadingProses = false;


  void getListWpSudahSidak(String keyword, String jenis_pekerjaan) async{
    this.loadingProses = true;
    update();

    Map<String, dynamic> params = {};
    params["user_id"] = SessionConfig.user_id;
    params["keyword"] = keyword;
    params["jenis_pekerjaan"] = jenis_pekerjaan;

    var request = await http
        .post(Uri.parse(Api.route[ModulesConfig.MODULE_PERMIT][Routes.GET_LIST_SIDAK_SEARCH]), body: params);

    if(request.statusCode == 200){
      this.loadingProses = false;
      update();

      var data = json.decode(request.body);
      for(var val in data["data"]){
        this.data.add(DaftarWpModel.fromJson(val));
      }

    }else{
      this.loadingProses = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data", backgroundColor: Colors.redAccent, colorText: Colors.white);
    }


    this.update();
  }
}