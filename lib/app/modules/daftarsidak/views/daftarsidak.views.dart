import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:get/get.dart';
import 'package:simson/app/modules/daftarsidak/controllers/daftarsidak.controllers.dart';
import 'package:simson/app/modules/daftarsidak/views/searchsidak.views.dart';
import 'package:simson/app/modules/wpdetail/views/wpdetails.views.dart';
import 'package:simson/ui/Uicolor.dart';

class DaftarSidakViews extends StatefulWidget {
  String jenisPekerjaan;
  DaftarSidakViews({required this.jenisPekerjaan});

  @override
  _DaftarSidakViewsState createState() => _DaftarSidakViewsState();
}

class _DaftarSidakViewsState extends State<DaftarSidakViews> {
  late DaftarSidakControllers controller;

  @override
  void initState() {
    controller = new DaftarSidakControllers();
    controller.getListWpSudahSidak(widget.jenisPekerjaan);
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text("${widget.jenisPekerjaan}", style: TextStyle(color: Uicolor.hexToColor(Uicolor.white)),),
          backgroundColor: Uicolor.hexToColor(Uicolor.green),
          elevation: 0,
          centerTitle: true,
        ),
        body: Container(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                    child: GetBuilder<DaftarSidakControllers>(
                      init: controller,
                      builder: (params){
                        if(!params.loadingProses){
                          if(params.data.length > 0){
                            return ListView.builder(
                              itemCount: params.data.length,
                              shrinkWrap: true,
                              physics: ClampingScrollPhysics(),
                              itemBuilder: (context, index){
                                var data = params.data[index];
                                return Container(
                                    height: MediaQuery.of(context).size.height * 0.12,
                                    margin: EdgeInsets.only(left: 16, right: 18, top: 8),
                                    child: GestureDetector(
                                      child: Card(
                                        elevation: 0,
                                        color: Uicolor.hexToColor(Uicolor.green_smooth),
                                        child: Container(
                                          padding: EdgeInsets.all(16),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Container(
                                                  child: Icon(Icons.check_circle, color: Uicolor.hexToColor(Uicolor.green),)
                                              ),
                                              Container(
                                                child: Text(data.no_wp),
                                              ),
                                              Container(
                                                child: Text(data.jam_sidak),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      onTap: ()=>{
                                        Get.to(WpDetailViews(no_wp: data.no_wp, VIEW_DETAIL: true, jenis_pekerjaan: widget.jenisPekerjaan, no_transaksi: data.no_trans,))
                                      },
                                    )
                                );
                              },
                            );
                          }
                        }

                        if(params.data.length == 0){
                          return Center(
                            child: Text("Tidak ada data ditemukan"),
                          );
                        }

                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      },
                    )
                ),

                Container(
                  child: RaisedButton(
                    child: Icon(Icons.refresh, color: Colors.white,),
                    color: Uicolor.hexToColor(Uicolor.green),
                    onPressed: (){
                      controller.getListWpSudahSidak(widget.jenisPekerjaan);
                    },
                    elevation: 3,
                    shape:CircleBorder(),
                  ),
                )
              ],
            )
          ),
        ),
        floatingActionButton: SpeedDial(
          backgroundColor: Colors.orangeAccent,
          children: [
            SpeedDialChild(
                backgroundColor: Colors.orangeAccent,
                child: Icon(Icons.search),
                onTap: (){
                  Get.to(SearchSidakViews(
                    jenis_pekerjaan: widget.jenisPekerjaan,
                  ));
                }
            ),
          ],
        )
    );
  }
}
