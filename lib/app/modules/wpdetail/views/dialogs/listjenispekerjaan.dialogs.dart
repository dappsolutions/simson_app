import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:simson/app/modules/sidak/views/sidak.views.dart';

class ListJenisPekerjaanDialogsViews extends StatefulWidget {
  double lat;
  double lng;
  String no_wp;
  ListJenisPekerjaanDialogsViews({required this.no_wp,required this.lat,required this.lng});

  @override
  _ListJenisPekerjaanDialogsViewsState createState() => _ListJenisPekerjaanDialogsViewsState();
}

class _ListJenisPekerjaanDialogsViewsState extends State<ListJenisPekerjaanDialogsViews> {
  List<String> dataPekerjaan = [];
  @override
  void initState() {
    dataPekerjaan.add("Relay dan Panel Kontrol");
    dataPekerjaan.add("Sipil");
    dataPekerjaan.add("Ruang Batrai");
    dataPekerjaan.add("Instalasi 20kV");
    dataPekerjaan.add("Transmisi");
    dataPekerjaan.add("Switchyard & MTU");
    dataPekerjaan.add("Survey ROW");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(16),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: 30,
              child: Text("Pilih Jenis Pekerjaan", style: TextStyle(fontSize: 18),),
            ),
            Container(
              margin: EdgeInsets.only(top: 16),
              child: ListView.builder(
                itemCount: dataPekerjaan.length,
                shrinkWrap: true,
                itemBuilder: (context, index){
                  return GestureDetector(
                      child: Container(
                        height: 45,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(dataPekerjaan[index]),
                            Divider()
                          ],
                        ),
                      ),
                    onTap: (){
                        // print("KLIK ${dataPekerjaan[index]}");
                      Navigator.pop(context);
                      Get.to(SidakViews(
                        no_wp: widget.no_wp,
                        lat: widget.lat,
                        lng: widget.lng,
                        jenisPekerjaan: dataPekerjaan[index],
                      ));
                    },
                  );
                },
              ),
            )
          ],
        ),
      )
    );
  }
}
