

class DetailWpModel {
 late String id;
 late String no_wp;
 late String user;
 late String tanggal_wp;
 late String tipe_permit;
 late String file_spk;
 late String penanggung_jawab_pekerjaan;
 late String jabatan_penanggung_jawab;
 late String pengawas_k3;
 late String jabatan_pengawas_k3;
 late String pengawas_pekerjaan;
 late String jabatan_pengawas_pekerjaan;
 late String nama_pemohon;
 late String perusahaan;
 late String no_hp;
 late String no_telp;
 late String jabatan;
 late String alamat;
 late String tgl_awal;
 late String tgl_akhir;
 late String uraian_pekerjaan;
 late String lokasi_pekerjaan;
 late String tempat;
 late String tempat_kerja_upt;
 late String tempat_kerja_gardu;
 late String tempat_kerja_ultg;
 late String tempat_kerja_sutt;
 late String tempat_kerja_bay;
 late String tempat_kerja;
 late String keterangan_need_sistem;
 late String is_need_sistem;
 late String lat;
 late String lng;

  DetailWpModel({
    required this.id,
    required this.no_wp,
    required this.user,
    required this.tanggal_wp,
    required this.tipe_permit,
    required this.file_spk,
    required this.penanggung_jawab_pekerjaan,
    required this.jabatan_penanggung_jawab,
    required this.pengawas_k3,
    required this.jabatan_pengawas_k3,
    required this.pengawas_pekerjaan,
    required this.jabatan_pengawas_pekerjaan,
    required this.nama_pemohon,
    required this.perusahaan,
    required this.no_hp,
    required this.no_telp,
    required this.jabatan,
    required this.alamat,
    required this.tgl_awal,
    required this.tgl_akhir,
    required this.uraian_pekerjaan,
    required this.lokasi_pekerjaan,
    required this.tempat,
    required this.tempat_kerja_upt,
    required this.tempat_kerja_gardu,
    required this.tempat_kerja_ultg,
    required this.tempat_kerja_sutt,
    required this.tempat_kerja_bay,
    required this.tempat_kerja,
    required this.keterangan_need_sistem,
    required this.is_need_sistem,
    required this.lat,
    required this.lng
  });


  DetailWpModel.fromJson(Map<String, dynamic> json){
    this.id = json['id'];
    this.no_wp = json['no_wp'];
    this.user = json['user'];
    this.tanggal_wp = json['tanggal_wp'];
    this.tipe_permit = json['tipe_permit'];
    this.file_spk = json['file_spk'];
    this.penanggung_jawab_pekerjaan = json['penanggung_jawab_pekerjaan'];
    this.jabatan_penanggung_jawab = json['jabatan_penanggung_jawab'];
    this.pengawas_k3 = json['pengawas_k3'];
    this.jabatan_pengawas_k3 = json['jabatan_pengawas_k3'];
    this.pengawas_pekerjaan = json['pengawas_pekerjaan'];
    this.jabatan_pengawas_pekerjaan = json['jabatan_pengawas_pekerjaan'];
    this.nama_pemohon = json['nama_pemohon'];
    this.perusahaan = json['perusahaan'];
    this.no_hp = json['no_hp'];
    this.no_telp = json['no_telp'];
    this.jabatan = json['jabatan'];
    this.alamat = json['alamat'];
    this.tgl_awal = json['tgl_awal'];
    this.tgl_akhir = json['tgl_akhir'];
    this.uraian_pekerjaan = json['uraian_pekerjaan'];
    this.lokasi_pekerjaan = json['lokasi_pekerjaan'];
    this.tempat = json['tempat'];
    this.tempat_kerja_upt = json['tempat_kerja_upt'] == null ? '' : json['tempat_kerja_upt'];
    this.tempat_kerja_gardu = json['tempat_kerja_gardu'];
    this.tempat_kerja_ultg = json['tempat_kerja_ultg'] == null ? '' : json['tempat_kerja_ultg'];
    this.tempat_kerja_sutt = json['tempat_kerja_sutt'];
    this.tempat_kerja_bay = json['tempat_kerja_bay'];
    this.tempat_kerja = json['tempat_kerja'];
    this.keterangan_need_sistem = json['keterangan_need_sistem'] == null ? '' : json['keterangan_need_sistem'];
    this.is_need_sistem = json['is_need_sistem'];
    this.lat = json['lat'] == null ? '' : json['lat'];
    this.lng = json['lng'] == null ? '' : json['lng'];
  }
}