

import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
// import 'package:location/location.dart';
import 'package:simson/app/modules/wpdetail/models/detailwp.models.dart';
import 'package:http/http.dart' as http;
import 'package:simson/config/api.config.dart';
import 'package:simson/config/modules.config.dart';
import 'dart:convert';

import 'package:simson/config/routes.config.dart';

class WpDetailControllers extends GetxController{

  List<DetailWpModel> data = [];
  bool loadingProses = false;

  double currentLatitudePos = 0;
  double currentLngitudePos = 0;
  // Location location;
  // PolylinePoints polylinePoints;
  // LocationData currentLocation;
  // Position posArea = null;

  void getDetailDataWp(String no_wp, bool view_detail) async{
    this.loadingProses = true;
    update();

    Map<String, dynamic> params = {};
    params["no_wp"] = no_wp;

    var request = await http
        .post(Uri.parse(Api.route[ModulesConfig.MODULE_PERMIT][Routes.GET_DETAIL_DATA]), body: params);

    if(request.statusCode == 200){
      this.loadingProses = false;
      update();

      var data = json.decode(request.body);
      // print("DATA ${data}");
      this.data.add(DetailWpModel.fromJson(data['data']));
      if(view_detail){
        this.setCurrentLocateWithData();
      }
    }else{
      this.loadingProses = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data", backgroundColor: Colors.redAccent, colorText: Colors.white);
    }


    this.update();
  }

  Future<Position> determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    // return await Geolocator.getCurrentPosition();
    Position posArea = await Geolocator.getCurrentPosition();
    return posArea;
  }

  Future<void> setCurrentLocate(BuildContext context) async{
    Position posArea = await this.determinePosition();
    this.currentLatitudePos = posArea.latitude;
    this.currentLngitudePos = posArea.longitude;
    // print("DATA_NOT_DETAIL LAT ${this.currentLatitudePos}, LNG ${this.currentLngitudePos}");
    this.update();
  }

  void setCurrentLocateWithData(){
    this.currentLatitudePos = this.data.length > 0 ? double.parse(this.data[0].lat) : 0;
    this.currentLngitudePos = this.data.length > 0 ? double.parse(this.data[0].lng) : 0;

    // print("DATA ALL ${this.data[0]}");
    print("DATA LAT ${this.currentLatitudePos}, LNG ${this.currentLngitudePos}");
    this.update();
  }
}