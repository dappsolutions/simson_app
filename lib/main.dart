import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:simson/app/modules/login/views/login.views.dart';
import 'package:simson/app/modules/main/views/main.views.dart';
import 'package:simson/ui/Uicolor.dart';
import 'package:simson/ui/Uititle.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: Uititle.title_apps,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MainViews()
    );
  }
}
