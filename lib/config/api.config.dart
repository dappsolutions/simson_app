import 'package:simson/config/modules.config.dart';
import 'package:simson/config/routes.config.dart';
import 'package:simson/config/url.config.dart';

class Api {
  static Map route = {
    ModulesConfig.MODULE_LOGIN: {
      Routes.SIGN_IN:
          "${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_LOGIN}/${Routes.SIGN_IN}"
    },
    ModulesConfig.MODULE_PERMIT: {
      Routes.GETDATA:
          "${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_PERMIT}/${Routes.GETDATA}",
      Routes.GETDATASEARCH:
          "${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_PERMIT}/${Routes.GETDATASEARCH}",
      Routes.GET_DETAIL_DATA:
          "${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_PERMIT}/${Routes.GET_DETAIL_DATA}",
      Routes.GET_LIST_SIDAK:
          "${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_PERMIT}/${Routes.GET_LIST_SIDAK}",
      Routes.GET_LIST_SIDAK_SEARCH:
          "${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_PERMIT}/${Routes.GET_LIST_SIDAK_SEARCH}",
    },
    ModulesConfig.MODULE_SIDAK: {
      Routes.SIMPAN:
          "${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_SIDAK}/${Routes.SIMPAN}",
      Routes.SIMPANALL:
          "${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_SIDAK}/${Routes.SIMPANALL}",
      Routes.BATAL:
          "${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_SIDAK}/${Routes.BATAL}",
      Routes.SIMPANGAMBAR:
          "${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_SIDAK}/${Routes.SIMPANGAMBAR}",
      Routes.SELESAIPROSES:
          "${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_SIDAK}/${Routes.SELESAIPROSES}",
      Routes.CEKSCORE:
          "${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_SIDAK}/${Routes.CEKSCORE}",
      Routes.GET_DETAIL_DATA:
          "${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_SIDAK}/${Routes.GET_DETAIL_DATA}",
      Routes.GET_DETAIL_IMAGE:
          "${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_SIDAK}/${Routes.GET_DETAIL_IMAGE}",
    },
    ModulesConfig.MODULE_APPS: {
      Routes.GETVERSION:
          "${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_APPS}/${Routes.GETVERSION}",
    },
  };
}
