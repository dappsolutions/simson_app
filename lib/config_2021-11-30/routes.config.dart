
class Routes{
  static String SIGN_IN = "signIn";
  static String GETDATA = "getData";
  static String GETDATASEARCH = "getDataSearch";
  static String GET_DETAIL_DATA = "getDetailData";
  static String SIMPAN = "simpan";
  static String CEKSCORE = "cekHasilPenilaianScoreSafety";
  static String GET_LIST_SIDAK = "getListWpSudahSidak";
  static String GET_LIST_SIDAK_SEARCH = "getListWpSudahSidakSearch";
  static String GET_DETAIL_IMAGE = "getDetailImage";
}