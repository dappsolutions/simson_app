// GENERATED CODE - DO NOT MODIFY BY HAND
// Generated and consumed by 'simple_json' 

import 'package:simple_json_mapper/simple_json_mapper.dart';
import 'package:simson/app/modules/login/storage/login.storage.dart';
import 'package:simson/app/modules/sidak/models/apd.models.dart';
import 'package:simson/app/modules/sidak/models/conditions.models.dart';
import 'package:simson/app/modules/sidak/storage/image.storage.dart';

final _loginstorageMapper = JsonObjectMapper(
  (CustomJsonMapper mapper, Map<String, dynamic> json) => LoginStorage(
    username: mapper.applyFromJsonConverter(json['username']),
    password: mapper.applyFromJsonConverter(json['password']),
  ),
  (CustomJsonMapper mapper, LoginStorage instance) => <String, dynamic>{
    'username': mapper.applyFromInstanceConverter(instance.username),
    'password': mapper.applyFromInstanceConverter(instance.password),
  },
);


final _apdmodelMapper = JsonObjectMapper(
  (CustomJsonMapper mapper, Map<String, dynamic> json) => ApdModel(
    id: mapper.applyFromJsonConverter(json['id']),
    body_harnes: mapper.applyFromJsonConverter(json['body_harnes']),
    lanyard: mapper.applyFromJsonConverter(json['lanyard']),
    hook_or_step: mapper.applyFromJsonConverter(json['hook_or_step']),
    pullstrap: mapper.applyFromJsonConverter(json['pullstrap']),
    safety_helmet: mapper.applyFromJsonConverter(json['safety_helmet']),
    kacamata_uv: mapper.applyFromJsonConverter(json['kacamata_uv']),
    earplug: mapper.applyFromJsonConverter(json['earplug']),
    masker: mapper.applyFromJsonConverter(json['masker']),
    wearpack: mapper.applyFromJsonConverter(json['wearpack']),
    sepatu_safety: mapper.applyFromJsonConverter(json['sepatu_safety']),
    sarung_tangan: mapper.applyFromJsonConverter(json['sarung_tangan']),
    stick_grounding: mapper.applyFromJsonConverter(json['stick_grounding']),
    grounding_cable: mapper.applyFromJsonConverter(json['grounding_cable']),
    stick_isolasi: mapper.applyFromJsonConverter(json['stick_isolasi']),
    voltage_detector: mapper.applyFromJsonConverter(json['voltage_detector']),
    scaffolding: mapper.applyFromJsonConverter(json['scaffolding']),
    tambang: mapper.applyFromJsonConverter(json['tambang']),
    tagging: mapper.applyFromJsonConverter(json['tagging']),
    pembatas_area: mapper.applyFromJsonConverter(json['pembatas_area']),
  ),
  (CustomJsonMapper mapper, ApdModel instance) => <String, dynamic>{
    'id': mapper.applyFromInstanceConverter(instance.id),
    'body_harnes': mapper.applyFromInstanceConverter(instance.body_harnes),
    'lanyard': mapper.applyFromInstanceConverter(instance.lanyard),
    'hook_or_step': mapper.applyFromInstanceConverter(instance.hook_or_step),
    'pullstrap': mapper.applyFromInstanceConverter(instance.pullstrap),
    'safety_helmet': mapper.applyFromInstanceConverter(instance.safety_helmet),
    'kacamata_uv': mapper.applyFromInstanceConverter(instance.kacamata_uv),
    'earplug': mapper.applyFromInstanceConverter(instance.earplug),
    'masker': mapper.applyFromInstanceConverter(instance.masker),
    'wearpack': mapper.applyFromInstanceConverter(instance.wearpack),
    'sepatu_safety': mapper.applyFromInstanceConverter(instance.sepatu_safety),
    'sarung_tangan': mapper.applyFromInstanceConverter(instance.sarung_tangan),
    'stick_grounding': mapper.applyFromInstanceConverter(instance.stick_grounding),
    'grounding_cable': mapper.applyFromInstanceConverter(instance.grounding_cable),
    'stick_isolasi': mapper.applyFromInstanceConverter(instance.stick_isolasi),
    'voltage_detector': mapper.applyFromInstanceConverter(instance.voltage_detector),
    'scaffolding': mapper.applyFromInstanceConverter(instance.scaffolding),
    'tambang': mapper.applyFromInstanceConverter(instance.tambang),
    'tagging': mapper.applyFromInstanceConverter(instance.tagging),
    'pembatas_area': mapper.applyFromInstanceConverter(instance.pembatas_area),
  },
);


final _conditionmodelMapper = JsonObjectMapper(
  (CustomJsonMapper mapper, Map<String, dynamic> json) => ConditionModel(
    id: mapper.applyFromJsonConverter(json['id']),
    kondisi_tanah: mapper.applyFromJsonConverter(json['kondisi_tanah']),
    kondisi_lingkungan: mapper.applyFromJsonConverter(json['kondisi_lingkungan']),
    kondisi_cuaca: mapper.applyFromJsonConverter(json['kondisi_cuaca']),
    ketinggian_tk: mapper.applyFromJsonConverter(json['ketinggian_tk']),
    tegangan_tk: mapper.applyFromJsonConverter(json['tegangan_tk']),
    lokasi: mapper.applyFromJsonConverter(json['lokasi']),
    status: mapper.applyFromJsonConverter(json['status']),
    score: mapper.applyFromJsonConverter(json['score']),
    nilai_apd: mapper.applyFromJsonConverter(json['nilai_apd']),
    resiko_pekerjaan: mapper.applyFromJsonConverter(json['resiko_pekerjaan']),
  ),
  (CustomJsonMapper mapper, ConditionModel instance) => <String, dynamic>{
    'id': mapper.applyFromInstanceConverter(instance.id),
    'kondisi_tanah': mapper.applyFromInstanceConverter(instance.kondisi_tanah),
    'kondisi_lingkungan': mapper.applyFromInstanceConverter(instance.kondisi_lingkungan),
    'kondisi_cuaca': mapper.applyFromInstanceConverter(instance.kondisi_cuaca),
    'ketinggian_tk': mapper.applyFromInstanceConverter(instance.ketinggian_tk),
    'tegangan_tk': mapper.applyFromInstanceConverter(instance.tegangan_tk),
    'lokasi': mapper.applyFromInstanceConverter(instance.lokasi),
    'status': mapper.applyFromInstanceConverter(instance.status),
    'score': mapper.applyFromInstanceConverter(instance.score),
    'nilai_apd': mapper.applyFromInstanceConverter(instance.nilai_apd),
    'resiko_pekerjaan': mapper.applyFromInstanceConverter(instance.resiko_pekerjaan),
  },
);


final _imagestorageMapper = JsonObjectMapper(
  (CustomJsonMapper mapper, Map<String, dynamic> json) => ImageStorage(
    encodeImages: mapper.applyFromJsonConverter(json['encodeImages']),
  ),
  (CustomJsonMapper mapper, ImageStorage instance) => <String, dynamic>{
    'encodeImages': mapper.applyFromInstanceConverter(instance.encodeImages),
  },
);

void init() {
  JsonMapper.register(_loginstorageMapper);
  JsonMapper.register(_apdmodelMapper);
  JsonMapper.register(_conditionmodelMapper);
  JsonMapper.register(_imagestorageMapper); 

  

  JsonMapper.registerListCast((value) => value?.cast<LoginStorage>()?.toList());
  JsonMapper.registerListCast((value) => value?.cast<ApdModel>()?.toList());
  JsonMapper.registerListCast((value) => value?.cast<ConditionModel>()?.toList());
  JsonMapper.registerListCast((value) => value?.cast<ImageStorage>()?.toList());
}
    